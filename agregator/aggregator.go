package agregator

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"
	"time"
)

/*

	aggregators accumulates maps in a buffer until a timeout

*/

type Aggregator interface {
	Run(ctx context.Context)
	Feed(map[string]string)
}

type GenericAggregator struct {
	Id     string
	Input  chan map[string]string
	Buffer []map[string]string

	Closed   bool
	Duration time.Duration // duration of the aggregation

	exit func(string)

	// exporter Exporter

}

func NewGenericAggregator(id string, duration time.Duration, exit func(string)) (a *GenericAggregator, err error) {

	a = &GenericAggregator{
		Id:    id,
		Input: make(chan map[string]string),
		//Buffer: make( []map[string]string ),
		Duration: duration,
		exit:     exit,
	}
	return
}

//
// implements Aggregator interface
//

func (a *GenericAggregator) Run(context.Context) {

	//log.Printf("Aggregator [%s] started\n", a.Id)

	for {
		select {

		case e := <-a.Input:
			a.Buffer = append(a.Buffer, e)

		case <-time.After(a.Duration):
			//log.Printf("aggregator [%s] exited\n", a.Id)
			a.Closed = true
			close(a.Input)
			a.Dump(os.Stdout)
			a.exit(a.Id)
			return
		}
	}
	//time.Sleep(a.Duration)
	//log.Printf("aggrator exited")

}

func (a *GenericAggregator) Feed(entry map[string]string) {
	if a.Closed == false {
		a.Input <- entry
	}
}

// utility function
func (a *GenericAggregator) Dump(w io.Writer) (err error) {

	var outs []string
	if len(a.Buffer) >= 3 {

		outs = append(outs, fmt.Sprintf("Dump aggregator %s", a.Id))
		for _, e := range a.Buffer {
			out, err := json.Marshal(e)
			if err == nil {
				outs = append(outs, string(out))
			} else {
				outs = append(outs, `nul`)
			}
		}
		result := strings.Join(outs, "\n")
		result += "\n"

		w.Write([]byte(result))
	}
	return
}
