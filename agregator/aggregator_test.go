package agregator

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"
)

func exit(id string) {
	fmt.Printf("exiting: %s\n", id)
}

func TestGenericAggregator(t *testing.T) {

	a, _ := NewGenericAggregator("A1", 5*time.Second, exit)

	e1 := map[string]string{"key": "value1"}
	e2 := map[string]string{"key": "value2"}

	go a.Run(context.Background())

	for {
		a.Feed(e1)
		a.Feed(e2)
		break
	}

	time.Sleep(1 * time.Second)
	_ = a.Dump(os.Stdout)

	time.Sleep(6 * time.Second)

	println("Done")

}
