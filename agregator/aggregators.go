package agregator

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"sync"
	"time"
)

// a map of aggregators
type AggregatorMap struct {
	Ctx context.Context

	mux      sync.Mutex
	Map      map[string]Aggregator
	Duration time.Duration

	// a channel of id ( sent by the aggregator to tell they are out )
	ClosedAggregator chan string
}

func NewAggregatorMap(duration time.Duration) (m *AggregatorMap) {
	m = &AggregatorMap{
		Ctx:              context.Background(),
		Map:              make(map[string]Aggregator),
		Duration:         duration,
		ClosedAggregator: make(chan string, 128),
	}
	return
}

// create an aggregator
func (m *AggregatorMap) New(id string, duration time.Duration) (a Aggregator, err error) {

	return NewGenericAggregator(id, duration, m.Delete)

}

func (m *AggregatorMap) Feed(id string, entry map[string]string) {

	// search if aggregator for id exists
	m.mux.Lock()
	a, ok := m.Map[id]
	if ok == false {
		// aggregator does not exists : create , run and feed
		n, _ := m.New(id, m.Duration)
		m.Map[id] = n
		m.mux.Unlock()
		go n.Run(m.Ctx)
		time.Sleep(1 * time.Millisecond)
		n.Feed(entry)
	} else {
		// feed the aggregator
		m.mux.Unlock()
		a.Feed(entry)
	}
}

func (m *AggregatorMap) Run(ctx context.Context) {

	for {
		// a task to delete closed aggregators
		select {
		case agId := <-m.ClosedAggregator:
			m.mux.Lock()
			//log.Printf("aggregatorMap deletes entry: %s", agId)
			delete(m.Map, agId)
			m.mux.Unlock()
		}
	}
}

func (m *AggregatorMap) Delete(agId string) {

	m.ClosedAggregator <- agId
}

// aggregator call this method on exit
func (m *AggregatorMap) ExitCallBack(agId string, data []map[string]string) {

	// ask for closing aggregator
	m.ClosedAggregator <- agId

	//  extract and publish  aggregate
	var outs []string
	if len(data) >= 3 {

		outs = append(outs, fmt.Sprintf("Dump aggregator %s", a.Id))
		for _, e := range data {
			out, err := json.Marshal(e)
			if err == nil {
				outs = append(outs, string(out))
			} else {
				outs = append(outs, `nul`)
			}
		}
		result := strings.Join(outs, "\n")
		result += "\n"

		println([]byte(result))
	}

}
