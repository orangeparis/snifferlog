package agregator

import (
	"testing"
	"time"
)

var data = []map[string]string{
	map[string]string{"id": "A", "key": "value1"},
	map[string]string{"id": "B", "key": "value2"},
	map[string]string{"id": "A", "key": "value3"},
	map[string]string{"id": "A", "key": "value4"},
	map[string]string{"id": "C", "key": "value5"},
	map[string]string{"id": "A", "key": "value6"},
}

func TestAggregatorMap(t *testing.T) {

	am := NewAggregatorMap(5 * time.Second)
	go am.Run(am.Ctx)

	for _, entry := range data {
		id := entry["id"]
		am.Feed(id, entry)
	}

	time.Sleep(2 * time.Second)

	//for k,v := range am.Map {
	//	log.Printf("Dump %s\n",k)
	//	_=v
	//	x := v.(GenericAggregator)
	//	//v.Dump()
	//}

	time.Sleep(6 * time.Second)

	print("Done")

}
