

# log handling conformity

we have a reference file for logs

    samples/LogFonct.Log-20191005  1.09 gb

=== RUN   TestDecodeTicket
2019/11/14 18:30:14 error reading file ../../samples/LogFonct.log-20191005 : EOF
decode [2840858] lines in 9.224303293s--- PASS: TestDecodeTicket (9.22s)
PASS

starts:  2019-10-05T01:00:52.431+02:00
end:     2019-10-05T03:41:22.210+02:00

so a spread of 2h40  (9400 seconds )for  2 840 858 messages

rate is 302,218 message per second or 1 message each 3.3ms


Some stats on files LogFonct.log-20191005 and LogFonct.log-20191006


=== RUN   TestAggregate
2020/02/17 18:34:33 error reading Source FileSource(../samples/LogFonct.log-20191005) : EOF
== stats ===
aggregate of size: 1 : 3			(0%)
aggregate of size: 2 : 1053386			(85%)
aggregate of size: 3 : 1896			(0%)
aggregate of size: 4 : 180340			(14%)
aggregate of size: 5 : 1			(-)
aggregate of size: 10 : 43			(0%)
aggregate of size: 800 : 1			(-)
aggregate of size: 5800 : 1			(-)

number of logs: 2840859
number of aggregates: 1235671
Done.
--- PASS: TestAggregate (13.11s)
PASS



=== RUN   TestAggregate
2020/02/17 18:36:07 error reading Source FileSource(../samples/LogFonct.log-20191006) : EOF
== stats ===
aggregate of size: 1 : 36			(0%)
aggregate of size: 2 : 786328			(70%)
aggregate of size: 3 : 1728			(0%)
aggregate of size: 4 : 292100			(26%)
aggregate of size: 6 : 30488			(2%)
aggregate of size: 7 : 1			(-)
aggregate of size: 10 : 36			(0%)
aggregate of size: 1400 : 1			(-)
aggregate of size: 3200 : 1			(-)

number of logs: 2934172
number of aggregates: 1110719
Done.
--- PASS: TestAggregate (13.32s)
PASS


## test on The two files  LogFonct.log-20191005 + LogFonct.log-20191006

=== RUN   TestAggregateOn2files
== stats ===
aggregate of size: 1 : 39			(0%)
aggregate of size: 2 : 1255523			(65%)
aggregate of size: 3 : 3624			(0%)
aggregate of size: 4 : 404584			(21%)
aggregate of size: 5 : 1			(-)
aggregate of size: 6 : 191193			(9%)
aggregate of size: 7 : 1			(-)
aggregate of size: 8 : 59447			(3%)
aggregate of size: 10 : 79			(0%)
aggregate of size: 800 : 1			(-)
aggregate of size: 1400 : 1			(-)
aggregate of size: 3200 : 1			(-)
aggregate of size: 5800 : 1			(-)

number of logs: 5775030
number of aggregates: 1914495
Done.
--- PASS: TestAggregateOn2files (25.31s)
PASS



=== RUN   TestAggregateCategoryOn2files
2020/02/19 12:45:48 error reading Source composite : EOF
== category stats ===
aggregate of category: 0 : 3			(0%)
aggregate of category: 12 : 1			(-)
aggregate of category: 13 : 3			(0%)
aggregate of category: 100 : 1255522			(65%)
aggregate of category: 200 : 404581			(21%)
aggregate of category: 300 : 191190			(9%)
aggregate of category: 400 : 59447			(3%)
aggregate of category: 1000 : 38			(0%)
aggregate of category: 1100 : 3622			(0%)
aggregate of category: 1111 : 1			(-)
aggregate of category: 1112 : 3			(0%)
aggregate of category: 1122 : 1			(-)
aggregate of category: 1125 : 79			(0%)
aggregate of category: 40000 : 1			(-)
aggregate of category: 70000 : 1			(-)
aggregate of category: 160000 : 1			(-)
aggregate of category: 290000 : 1			(-)

number of logs: 5775030
number of aggregates: 1914495
Done.
--- PASS: TestAggregateCategoryOn2files (26.15s)
PASS


=== RUN   TestAggregateCategory
2020/02/19 18:32:25 error reading Source FileSource(../samples/LogFonct-ines.log) : EOF
== category stats ===
aggregate of category: 0 : 237913			(49%)
aggregate of category: 1000 : 6			(0%)
aggregate of category: 1011 : 3125			(0%)
aggregate of category: 1100 : 3			(0%)
aggregate of category: 1111 : 240955			(49%)
aggregate of category: 2222 : 7			(0%)
aggregate of category: 3333 : 11			(0%)

number of logs: 1211309
number of aggregates: 482020
Done.
--- PASS: TestAggregateCategory (7.17s)
PASS


=== RUN   TestAggregateCategory
2020/02/19 18:34:54 error reading Source FileSource(../samples/LogFonct.ines-1.log) : EOF
== category stats ===
aggregate of category: 0 : 260424			(52%)
aggregate of category: 11 : 1			(-)
aggregate of category: 100 : 10			(0%)
aggregate of category: 111 : 12			(0%)
aggregate of category: 1000 : 34			(0%)
aggregate of category: 1001 : 1			(-)
aggregate of category: 1011 : 1816			(0%)
aggregate of category: 1100 : 47			(0%)
aggregate of category: 1101 : 12			(0%)
aggregate of category: 1110 : 1			(-)
aggregate of category: 1111 : 230015			(46%)
aggregate of category: 2222 : 62			(0%)
aggregate of category: 3333 : 31			(0%)

number of logs: 1187026
number of aggregates: 492466
Done.
--- PASS: TestAggregateCategory (7.07s)
PASS