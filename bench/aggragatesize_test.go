package bench

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	snifferlog "bitbucket.org/orangeparis/snifferlog/pkg"
	"fmt"
	"log"
	"sort"
	"testing"
)

var aggSize map[string]int
var limit int = 10000000

func TestAggregate(t *testing.T) {

	// open file to read tickets
	filename := "../samples/LogFonct.log-20191006"

	argSize := make(map[string]int)

	source, err := sources.NewFileSource(filename, "")
	//source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		t.Fail()
		return
	}
	defer source.Close()

	// reading ticket loop
	n := 1
	for finished := false; finished == false; {
		data, err := source.Next()
		if err != nil {
			log.Printf("error reading Source %s : %s", source.Name(), err)
			finished = true
			break
		}

		ticket, err := snifferlog.NewTicket(data)
		if err != nil {
			log.Printf("Parser: failed to parse ticket (%s): %s\n", err.Error(), string(data))
			//p.PublishError(err.Error(), string(data))
			return
		}
		traceID := ticket.GetTraceID()
		if _, ok := argSize[traceID]; ok == false {
			argSize[traceID] = 1
		} else {
			argSize[traceID] += 1
		}

		n += 1
		if n > limit {
			println("limit encountered")
			break
		}
	}

	// result
	stats := make(map[int]int)

	for _, v := range argSize {

		if _, ok := stats[v]; ok == false {
			stats[v] = 1
		} else {
			stats[v] += 1
		}

		//if v > 2 {
		//	fmt.Printf("%s:%d\n", k, v)
		//}
	}

	Displaystats(n, argSize)

	//// display stats
	//println("== stats ===")
	//for k,v := range stats {
	//	fmt.Printf("aggregate of size: %d : %d\n",k,v)
	//}
	//
	//println()
	//fmt.Printf("number of logs: %d\n", n )
	//fmt.Printf("number of aggregates: %d\n", len(argSize) )

	println("Done.")

}

func TestAggregateOn2files(t *testing.T) {

	// open file to read tickets
	filename1 := "../samples/LogFonct.log-20191005"
	filename2 := "../samples/LogFonct.log-20191006"

	argSize := make(map[string]int)

	source, err := sources.NewFileSource(filename1, "")
	//source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		t.Fail()
		return
	}

	// reading ticket loop
	fileId := 1

	n := 1
	for finished := false; finished == false; {
		data, err := source.Next()
		if err != nil {
			if err.Error() == "EOF" {
				// end of file
				if fileId == 1 {
					// end of file 1 : open file2
					source, err = sources.NewFileSource(filename2, "")
					if err != nil {
						log.Println(err)
						t.Fail()
						return
					}
					fileId += 1
					continue
				} else {
					// end of file 2
					finished = true
					break
				}
			} else {
				// error which is not an EOF
				log.Printf("error reading Source %s : %s", source.Name(), err)
				finished = true
				break
			}
		}

		ticket, err := snifferlog.NewTicket(data)
		if err != nil {
			log.Printf("Parser: failed to parse ticket (%s): %s\n", err.Error(), string(data))
			//p.PublishError(err.Error(), string(data))
			return
		}
		traceID := ticket.GetTraceID()
		if _, ok := argSize[traceID]; ok == false {
			argSize[traceID] = 1
		} else {
			argSize[traceID] += 1
		}

		n += 1
		if n > limit {
			println("limit encountered")
			break
		}
	}

	// result
	stats := make(map[int]int)

	for _, v := range argSize {

		if _, ok := stats[v]; ok == false {
			stats[v] = 1
		} else {
			stats[v] += 1
		}
	}

	Displaystats(n, argSize)

	//// display stats
	//println("== stats ===")
	//keys := make([]int, 0, len(stats))
	//for k := range stats {
	//	keys = append(keys, k)
	//}
	//sort.Ints(keys)
	//
	////for _, k := range keys {
	////	fmt.Println(k, m[k])
	////}
	//
	//
	//for k,v := range stats {
	//	percent :=  "-"
	//	if v > 1 {
	//		// percentage
	//		percent = fmt.Sprintf("%d%%", v * 100 / len(argSize)  )
	//	}
	//	fmt.Printf("aggregate of size: %d : %d      (%s)\n",k,v,percent)
	//}
	//
	//println()
	//fmt.Printf("number of logs: %d\n", n )
	//fmt.Printf("number of aggregates: %d\n", len(argSize) )

	println("Done.")

}

func Displaystats(n int, argSize map[string]int) {

	// result
	stats := make(map[int]int)

	for _, v := range argSize {

		if _, ok := stats[v]; ok == false {
			stats[v] = 1
		} else {
			stats[v] += 1
		}

		//if v > 2 {
		//	fmt.Printf("%s:%d\n", k, v)
		//}
	}

	// display stats
	println("== stats ===")
	keys := make([]int, 0, len(stats))
	for k := range stats {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	for _, k := range keys {
		v := stats[k]
		percent := "-"
		if v > 1 {
			// percentage
			percent = fmt.Sprintf("%d%%", v*100/len(argSize))
		}
		fmt.Printf("aggregate of size: %d : %d\t\t\t(%s)\n", k, v, percent)
	}

	println()
	fmt.Printf("number of logs: %d\n", n)
	fmt.Printf("number of aggregates: %d\n", len(argSize))

}
