package bench

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	snifferlog "bitbucket.org/orangeparis/snifferlog/pkg"
	"fmt"
	"log"
	"testing"
)

func TestAggregateByLineID(t *testing.T) {

	// open file to read tickets
	filename := "../samples/LogFonct.log-20191006"

	argSize := make(map[string]int)

	source, err := sources.NewFileSource(filename, "")
	//source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		t.Fail()
		return
	}
	defer source.Close()

	// reading ticket loop
	n := 1
	for finished := false; finished == false; {
		data, err := source.Next()
		if err != nil {
			log.Printf("error reading Source %s : %s", source.Name(), err)
			finished = true
			break
		}

		ticket, err := snifferlog.NewTicket(data)
		if err != nil {
			log.Printf("Parser: failed to parse ticket (%s): %s\n", err.Error(), string(data))
			//p.PublishError(err.Error(), string(data))
			return
		}
		ticketCategory := snifferlog.TicketTags(ticket.Tags).GetCategory()
		if ticketCategory != 0 {
			lineID := ticket.GetLineID()
			if lineID != "" {
				if _, ok := argSize[lineID]; ok == false {
					argSize[lineID] = ticketCategory
				} else {
					argSize[lineID] += ticketCategory
				}
			}
		}

		n += 1
		if n > limit {
			println("limit encountered")
			break
		}
	}

	for k, v := range argSize {
		if v > 1000 {
			fmt.Printf("category of lineID %s is %d\n", k, v)
		}
	}

	// result
	stats := make(map[int]int)

	for _, v := range argSize {

		if _, ok := stats[v]; ok == false {
			stats[v] = 1
		} else {
			stats[v] += 1
		}
	}

	DisplayCategoryStats(n, argSize)

	println("Done.")

}
