package bench

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	snifferlog "bitbucket.org/orangeparis/snifferlog/pkg"
	"fmt"
	"log"
	"sort"
	"testing"
)

func TestAggregateCategory(t *testing.T) {

	// open file to read tickets
	filename := "../samples/LogFonct.log-20191005"
	// filename := "../samples/LogFonct-ines.log"
	//filename := "../samples/LogFonct.ines-1.log"

	argSize := make(map[string]int)

	source, err := sources.NewFileSource(filename, "")
	//source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		t.Fail()
		return
	}
	defer source.Close()

	// reading ticket loop
	n := 1
	for finished := false; finished == false; {
		data, err := source.Next()
		if err != nil {
			log.Printf("error reading Source %s : %s", source.Name(), err)
			finished = true
			break
		}

		ticket, err := snifferlog.NewTicket(data)
		if err != nil {
			log.Printf("Parser: failed to parse ticket (%s): %s\n", err.Error(), string(data))
			//p.PublishError(err.Error(), string(data))
			return
		}
		ticketCategory := snifferlog.TicketTags(ticket.Tags).GetCategory()

		traceID := ticket.GetTraceID()
		if _, ok := argSize[traceID]; ok == false {
			argSize[traceID] = ticketCategory
		} else {
			argSize[traceID] += ticketCategory
		}

		n += 1
		if n > limit {
			println("limit encountered")
			break
		}
	}

	//for k, v := range argSize {
	//	if v > 1000 {
	//		fmt.Printf("category of %s is %d\n", k, v)
	//	}
	//}

	// result
	stats := make(map[int]int)

	for _, v := range argSize {

		if _, ok := stats[v]; ok == false {
			stats[v] = 1
		} else {
			stats[v] += 1
		}
	}
	DisplayCategoryStats(n, argSize)

	println("Done.")

}

func TestAggregateCategoryOn2files(t *testing.T) {

	// open file to read tickets
	delay := ""
	filename1 := "../samples/LogFonct.log-20191005"
	filename2 := "../samples/LogFonct.log-20191006"

	argSize := make(map[string]int)

	source, err := sources.NewFileSources(delay, filename1, filename2)
	//source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		t.Fail()
		return
	}
	//defer source.Close()

	// reading ticket loop
	n := 1
	for finished := false; finished == false; {
		data, err := source.Next()
		if err != nil {
			log.Printf("error reading Source %s : %s", source.Name(), err)
			finished = true
			break
		}

		ticket, err := snifferlog.NewTicket(data)
		if err != nil {
			log.Printf("Parser: failed to parse ticket (%s): %s\n", err.Error(), string(data))
			//p.PublishError(err.Error(), string(data))
			return
		}
		ticketCategory := snifferlog.TicketTags(ticket.Tags).GetCategory()

		traceID := ticket.GetTraceID()
		if _, ok := argSize[traceID]; ok == false {
			argSize[traceID] = ticketCategory
		} else {
			argSize[traceID] += ticketCategory
		}

		n += 1
		//if n > limit {
		//	println("limit encountered")
		//	break
		//}
	}

	//for k, v := range argSize {
	//	if v > 1000 {
	//		fmt.Printf("category of %s is %d\n", k, v)
	//	}
	//}

	// result
	stats := make(map[int]int)

	for _, v := range argSize {

		if _, ok := stats[v]; ok == false {
			stats[v] = 1
		} else {
			stats[v] += 1
		}
	}
	DisplayCategoryStats(n, argSize)

	println("Done.")

}

func DisplayCategoryStats(n int, argSize map[string]int) {

	// result
	stats := make(map[int]int)

	for _, v := range argSize {

		if _, ok := stats[v]; ok == false {
			stats[v] = 1
		} else {
			stats[v] += 1
		}

		//if v > 2 {
		//	fmt.Printf("%s:%d\n", k, v)
		//}
	}

	// display stats
	println("== category stats ===")
	keys := make([]int, 0, len(stats))
	for k := range stats {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	for _, k := range keys {
		v := stats[k]
		percent := "-"
		if v > 1 {
			// percentage
			percent = fmt.Sprintf("%d%%", v*100/len(argSize))
		}
		fmt.Printf("aggregate of category: %d : %d\t\t\t(%s)\n", k, v, percent)
	}

	println()
	fmt.Printf("number of logs: %d\n", n)
	fmt.Printf("number of aggregates: %d\n", len(argSize))

}
