package bench

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	snifferlog "bitbucket.org/orangeparis/snifferlog/pkg"
	"fmt"

	//"fmt"
	"log"
	"testing"
)

//
// is tid belong to only one emitter
//
func TestTidByEmitter(t *testing.T) {

	// open file to read tickets
	filename := "../samples/LogFonct.log-20191006"

	//
	tidEmitterMap := make(map[string]map[string]int)

	source, err := sources.NewFileSource(filename, "")
	//source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		t.Fail()
		return
	}
	defer source.Close()

	// reading ticket loop
	//n := 1
	for finished := false; finished == false; {
		data, err := source.Next()
		if err != nil {
			log.Printf("error reading Source %s : %s", source.Name(), err)
			finished = true
			break
		}

		ticket, err := snifferlog.NewTicket(data)
		if err != nil {
			log.Printf("Parser: failed to parse ticket (%s): %s\n", err.Error(), string(data))
			//p.PublishError(err.Error(), string(data))
			return
		}
		ticketCategory := snifferlog.TicketTags(ticket.Tags).GetCategory()
		if ticketCategory != 0 {
			tid := ticket.GetTraceID()
			emitter := ticket.Header.Origin

			if _, ok := tidEmitterMap[tid]; ok != true {
				// tid id entry does not exists
				tidEmitterMap[tid] = make(map[string]int)
			}
			if _, ok := tidEmitterMap[tid][emitter]; ok != true {
				// tid id entry does not exists
				tidEmitterMap[tid][emitter] = 0
			}
			tidEmitterMap[tid][emitter] += 1

		}

		//n += 1
		//if n > limit {
		//	println("limit encountered")
		//	break
		//}
	}

	for k, v := range tidEmitterMap {
		if len(v) > 1 {
			fmt.Printf("more than one origin for tid: %s\n", k)
		}
	}

	// result
	//stats := make(map[int]int)
	//
	//for _, v := range argSize {
	//
	//	if _, ok := stats[v]; ok == false {
	//		stats[v] = 1
	//	} else {
	//		stats[v] += 1
	//	}
	//}

	//DisplayCategoryStats(n, argSize)

	//println("Done.")
	print()

}
