package bench

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	snifferlog "bitbucket.org/orangeparis/snifferlog/pkg"
	"fmt"
	"log"
	"os"
	"testing"
	"time"
)

type TimeSpread struct {
	Count      int
	Start      string
	End        string
	Spread     time.Duration
	FirstLine  int
	LastLine   int
	LineSpread int
}

func TestTimeSpread(t *testing.T) {

	// open file to read tickets
	//filename := "../samples/LogFonct-ines.log"
	filename := "../samples/LogFonct-ines-short.log"

	spreadMap := make(map[string]*TimeSpread)
	//var entry *TimeSpread

	source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		t.Fail()
		return
	}
	defer source.Close()

	// reading ticket loop
	n := 1
	for finished := false; finished == false; {
		data, err := source.Next()
		if err != nil {
			log.Printf("error reading Source %s : %s", source.Name(), err)
			finished = true
			break
		}

		n += 1
		ticket, err := snifferlog.NewTicket(data)
		if err != nil {
			log.Printf("Parser: failed to parse ticket (%s): %s\n", err.Error(), string(data))
			//p.PublishError(err.Error(), string(data))
			return
		}

		tm := ticket.Header.TimeStamp

		traceID := ticket.GetTraceID()
		if entry, ok := spreadMap[traceID]; ok == false {
			// create entry
			entry = &TimeSpread{Count: 1, Start: tm, End: tm, FirstLine: n, LastLine: n}
			spreadMap[traceID] = entry

		} else {
			// update entry
			entry.Count += 1
			if tm < entry.Start {
				entry.Start = tm
			}
			if tm > entry.End {
				entry.End = tm
			}
			entry.LastLine = n
		}

		//if n > limit {
		//	println("limit encountered")
		//	break
		//}
	}

	fmt.Printf("number of logs: %d\n", n)
	fmt.Printf("number of aggregates: %d\n", len(spreadMap))

	DisplaySpreadTime(spreadMap)
	DisplaySpreadLine(spreadMap)

	println("Done.")

}

func DisplaySpreadTime(spreadMap map[string]*TimeSpread) {
	println("\n======== Display spread time")

	// compute spread
	var spreadPercentil = make(map[string]int)
	var max time.Duration

	for k, v := range spreadMap {
		t0, err := time.Parse(time.RFC3339, v.Start)
		if err != nil {
			log.Fatalf("bad timestamp: %s\n", t0)
		}
		t1, err := time.Parse(time.RFC3339, v.End)
		if err != nil {
			log.Fatalf("bad timestamp: %s\n", t1)
		}
		elapsed := t1.Sub(t0)
		if elapsed > max {
			max = elapsed
		}
		spreadMap[k].Spread = elapsed
		spreadMap[k].LineSpread = spreadMap[k].LastLine - spreadMap[k].FirstLine

		mark := elapsed.String()
		if _, ok := spreadPercentil[mark]; ok == false {
			// create mark
			spreadPercentil[mark] = 1
		} else {
			spreadPercentil[mark] += 1
		}

		//fmt.Printf("elapsed: %s\n",elapsed.String())
	}
	fmt.Printf("max elapsed: %s\n", max.String())

	for k, v := range spreadPercentil {
		fmt.Printf("spread value: %s : %d\n", k, v)
	}

}

func DisplaySpreadLine(spreadMap map[string]*TimeSpread) {

	println("\n======== Display spread line")

	// compute spread
	var spreadPercentil = make(map[int]int)
	var max int = 0

	for k, _ := range spreadMap {

		spreadMap[k].LineSpread = spreadMap[k].LastLine - spreadMap[k].FirstLine

		if spreadMap[k].LineSpread > max {
			max = spreadMap[k].LineSpread
		}
		index := spreadMap[k].LineSpread / 50

		if _, ok := spreadPercentil[index]; ok == false {
			// create mark
			spreadPercentil[index] = 1
		} else {
			spreadPercentil[index] += 1
		}

	}
	fmt.Printf("max line spread: %d\n", max)

	for k, v := range spreadPercentil {
		fmt.Printf("spread value: %d : %d\n", k*50, v)
	}

}

func TestSplitInesLog(t *testing.T) {

	// open file to read tickets
	filename := "../samples/LogFonct-ines.log"
	target := "../samples/LogFonct-ines-short.log"
	var targetHandler *os.File

	split := "2020-02-19T"

	source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		t.Fail()
		return
	}
	defer source.Close()

	// reading ticket loop
	//n := 1
	trigger := false
	for finished := false; finished == false; {
		data, err := source.Next()
		if err != nil {
			log.Printf("error reading Source %s : %s", source.Name(), err)
			finished = true
			break
		}
		if trigger == false {
			s := data[:len(split)]
			if string(s) == split {
				targetHandler, err = os.Create(target)
				if err != nil {
					log.Fatalf("%s\n", err.Error())
				}
				targetHandler.Write(data)
				trigger = true
			}
		} else {
			targetHandler.Write(data)
		}

	}
	targetHandler.Close()

}
