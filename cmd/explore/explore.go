package main

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	filters "bitbucket.org/orangeparis/ines/tagfilter"
	"bitbucket.org/orangeparis/snifferlog/internal/processors"
	"fmt"
	"log"
	"time"
)

/*

	a tool for analysing samples



*/

func main() {

	filename := "./samples/LogFonct.log-20191006"

	filterLogType := filters.NewTagListFilter("LogType", "FUNCT_TICKET_MQ")

	result := NewStats()

	/*
		stats["LogType"]= make(map[string]int)
		stats["MessType"]= make(map[string]int)
		stats["InformationalSystem"]= make(map[string]int)
		stats["ClientOption"]= make(map[string]int)
	*/
	//ClientOption

	start := time.Now()

	s, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Fatalf("%s", err.Error())
		return
	}

	count := 0
	for finished := false; finished == false; {
		data, err := s.Next()
		if err != nil {
			log.Printf("error reading file %s : %s", s.Filename, err)
			finished = true
			break
		}

		ticket, _ := processors.NewTicket(data)

		// add filter to keep only FUNCT_TICKET
		ok := filterLogType.Filter(ticket.Tags)
		if ok == false {
			continue
		}

		result.Feed("LogType", &ticket)
		result.Feed("MessType", &ticket)
		result.Feed("InformationalSystem", &ticket)
		result.Feed("ClientOption", &ticket)
		result.Count("Login", &ticket)
		result.Feed("BNGID", &ticket)
		result.Count("BNGIP", &ticket)
		result.Count("LineID", &ticket)
		result.Count("ErrCode", &ticket)
		result.Count("EventCause", &ticket)

		_ = ticket

		count++
		//fmt.Println(dic)
	}

	elapsed := time.Since(start)
	fmt.Printf("decode [%d] lines in %s\n", count, elapsed)
	//fmt.Printf("stats: %v\n", stats)

	fmt.Printf("stats: %v\n", string(result.Json()))

}

func feedStat(category string, stats map[string]map[string]int, ticket *processors.TicketInes) {

	// create category if necessary
	if _, ok := stats[category]; ok == false {
		stats[category] = make(map[string]int)
	}
	// count fields
	field, ok := ticket.Tags[category]
	if ok == true {
		_, ok := stats[category][field]
		if ok == true {
			stats[category][field] += 1
		} else {
			stats[category][field] += 1
		}
	}
}
