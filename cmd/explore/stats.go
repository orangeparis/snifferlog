package main

import (
	"bitbucket.org/orangeparis/snifferlog/internal/processors"
	"encoding/json"
	"log"
)

type Stats map[string]map[string]int

func NewStats() Stats {
	//s := make(map[string]map[string]int)
	s := make(Stats)
	return s
}

//// implements publisher
//func ( s Stats) Publish(topic string, message []byte) (err error) {
//
//	return err
//}

func (s Stats) Feed(category string, ticket *processors.TicketInes) {

	// create category if necessary
	if _, ok := s[category]; ok == false {
		s[category] = make(map[string]int)
	}
	// count fields
	field, ok := ticket.Tags[category]
	if ok == true {
		_, ok := s[category][field]
		if ok == true {
			s[category][field] += 1
		} else {
			s[category][field] += 1
		}
	}
}

func (s Stats) Count(category string, ticket *processors.TicketInes) {

	// create category if necessary
	if _, ok := s[category]; ok == false {
		s[category] = make(map[string]int)
	}
	// count fields without enum them
	_, ok := ticket.Tags[category]
	if ok == true {
		_, ok := s[category]["."]
		if ok == true {
			s[category]["."] += 1
		} else {
			s[category]["."] += 1
		}
	}
}

func (s *Stats) Json() []byte {
	//j,err := json.Marshal(s)
	j, err := json.MarshalIndent(s, "", "\t")

	if err != nil {
		log.Fatalf("json marshal failed: %s", err.Error())
	}
	return j
}
