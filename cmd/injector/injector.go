package main

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	"bitbucket.org/orangeparis/snifferlog/players"
	"flag"
	"fmt"
	"log"
	"os"
)

/*
	injector: create a linux pipe, dump content of a file in it

*/

var (
	Version = "0.1"

	fFile = flag.String("file", "", "rtlog file to read (required eg: sample.rtlog)")
	fPipe = flag.String("pipe", "/tmp/pipe.rtlog", "linux pipe")

	helper = `create a linux pipe and dump a file into it
sample usage : injector --file "mylog.rtlog" --pipe "/tmp/pipe.rtlog"
options:
--pipe string
    	linux pipe of rtlog lines ( eg /tmp/pipe.rtlog )
--file string
		rtlog file to read (eg: sample.rtlog)

`
)

func main() {

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, fmt.Sprintf("injector %s\n", Version))
		fmt.Fprintf(os.Stderr, helper)
	}

	flag.Parse()

	// create source
	// build source from file
	s, err := sources.NewFileSource(*fFile, "")
	if err != nil {
		log.Printf("failed to open file source: %s", *fFile)
		return
	}
	defer s.Close()

	p, err := players.NewPipeInjector(*fPipe, s, 0)
	if err != nil {
		log.Printf("failed to create pipe injector: %s", err.Error())
		return
	}
	defer p.Close()

	// write all file content to pipe
	log.Printf("start pipe writer to %s\n", p.File.Name())
	p.Start()

	select {}

}
