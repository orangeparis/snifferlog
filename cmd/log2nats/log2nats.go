package main

import (
	"bitbucket.org/orangeparis/ines/heartbeat"
	"bitbucket.org/orangeparis/ines/nping"
	"bitbucket.org/orangeparis/ines/publisher"
	"flag"
	"fmt"
	"log"
	"os"
)

var (
	Version = "0.1"

	name = flag.String("name", "", "name of the snifferlog (eg 0 , will publish to sniffer.0.>)")

	fPipe = flag.String("pipe", "", "linux pipe")
	fFile = flag.String("file", "", "log file to read (eg: sample.rtlog)")

	nats = flag.String("nats", "nats://127.0.0.1:4222", "nats server (eg nats://demo.nats.io:4222)")

	helper = `read from a linux pipe OR a rtlog file and send messages to nats server
sample usage : snifferlog --name innapnoi6fed04 --pipe /var/ines/pipe --nats nats://<host>:4222
options:
--name string
	name of the sniffer (default "0") (eg 0 , will publish to sniffer.0.>) 

--pipe string
    	linux pipe of rtlog lines
--file string
		rtlog file to read (eg: sample.rtlog)
		
--nats string
    	nats server (default "nats://127.0.0.1:4222")
`
)

func getPublisher() (pub publisher.Publisher, err error) {

	// return a publisher
	//  if --screen return a screen publisher
	//  else return a nats publisher

	// topic   eg sniffer.log  sniffer.log2
	var topic string

	//if strings.HasSuffix(*name, ".") {
	topic = fmt.Sprintf("logs.%s", *name)

	// a nats publisher
	natsURL := *nats
	pub, err = publisher.NewNatsPublisher(topic, natsURL)
	log.Printf("snifferlog : set Publisher to nats publisher at %s", natsURL)

	// add a heartbeat emitter Heartbeat.logs.
	e, _ := heartbeat.NewEmitter(topic, natsURL, 10)
	e.Start()
	log.Printf("log2nats : started a heartbeat emitter [Heartbeat.%s] at  %s", topic, natsURL)

	// handle call.
	c, err := nping.NewClient(*nats)
	if err != nil {
		log.Printf("log2nats : Warning: failed to start handler for call.%s.ping\n", topic)
		return pub, err
	}
	defer c.Close()
	if err == nil {
		//pingTopic := "call." + topic + ".ping"
		sub, err := c.HandlePingService(topic)
		if err != nil {
			log.Printf("log2nats : Warning: failed to subscribe to ping topic: %s\n", topic)
			return pub, err
		} else {
			defer sub.Unsubscribe()
			pingTopic := "call." + topic + ".ping"
			log.Printf("start handler for ping service at: %s\n", pingTopic)

		}

	}

	log.Printf("log2nats : set root topic to: %s\n", topic)

	return pub, err
}

func fileMode(pub publisher.Publisher, filename string) {

}

func pipeMode(pub publisher.Publisher, pipeName string) {

}

func run() {

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, fmt.Sprintf("snifferlog %s\n", Version))
		fmt.Fprintf(os.Stderr, helper)
	}

	flag.Parse()

	// get a publisher corresponding to the flags ( screen or nats )
	publisher, err := getPublisher()
	if err != nil {
		log.Fatalf("snifferlog: cannot get a publisher: %s\n", err.Error())
		os.Exit(1)
	}

	// find  mode ( pipe or file )
	mode := "PIPE"
	if *fFile != "" {
		mode = "FILE"
	}

	//log.Printf("sniffer: starting")
	switch mode {
	case "FILE":
		// read from a rtlog file
		log.Printf("sniffer: create a file reader with %s\n", *fFile)
		fileMode(publisher, *fFile)

	default:
		// read from network interface (need to be root )
		log.Printf("snifferlog : create a pipereader with %s\n", *fPipe)
		pipeMode(publisher, *fPipe)
	}
	log.Printf("snifferlog : exiting")

}

func main() {
	run()
}
