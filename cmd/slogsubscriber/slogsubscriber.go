package main

import (
	snifferlog "bitbucket.org/orangeparis/snifferlog/pkg"
	"context"
	"flag"
	"fmt"
	"log"
	"os"
)

var (
	Version = "0.1"

	name = flag.String("name", "log", "to sniffer.log.>)")
	line = flag.String("line", "", "to sniffer.log.<line>)")

	nats = flag.String("nats", "nats://127.0.0.1:4222", "nats server (eg nats://demo.nats.io:4222)")

	helper = `
sample usage : slogsubscriber --name log --line 0001234 --nats nats://<host>:4222
options:
--name string
	name of the sniffer (default "log") ( will subscribe to sniffer.log.>)
--line string
    	line id :  will subscribe to snifferlog.log
--nats string
    	nats server (default "nats://127.0.0.1:4222")
`
)

func run() {

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, fmt.Sprintf("snifferlog %s\n", Version))
		fmt.Fprintf(os.Stderr, helper)
	}

	flag.Parse()

	// set topic
	var topic string
	if *line != "" {
		// set topic to catch one line ID, eg sniffer.log.0123456
		topic = "sniffer." + *name + "." + *line
	} else {
		// set topic to catch all line  ( sniffer.log.>
		topic = "sniffer." + *name + ".>"

	}

	s, err := snifferlog.NewNatsSnifferLogSubscriber(*nats, topic)
	if err != nil {
		log.Fatalf("slogsubscriber failed: %s", err.Error())
		return
	}

	ctx := context.Background()
	log.Printf("slogsubscriber subscribes to : %s with topic: %s\n", *nats, topic)
	s.Run(ctx)

	log.Printf("slog subscriber : exiting")

}

func main() {
	run()
}
