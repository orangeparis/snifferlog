# snifferlog

a go executable to read ines tickets  and publish nats messages on topic sniffer.log.>

## usage

## read from linux pipe

    snifferlog --name 1 --pipe /var/ines/tickets --nats nats://127.0.0.1:4222 

read rtlog lines from linux pipe /var/ines/tickets
and publish message to nats server at nats://127.0.0.1:4222 
with subjects like  snifferlog.1.>

## read from  file

    sniffer --name 2 --file ="./sample.rtlog --nats: nats://127.0.0.1:4222 

read lines from  file sample.rtlog
and publish message to nats server nats://127.0.0.1:4222 
with subjects like sniffer.2.>

## read from  file , publish to screen for testing

    sniffer --name 2 --file="./sample.rtlog --screen

## read from pipe , publish to screen for testing

    sniffer --name 1 --pipe /var/ines/tickets --screen 
