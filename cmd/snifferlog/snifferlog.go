package main

import (
	"bitbucket.org/orangeparis/ines/publisher"
	"bitbucket.org/orangeparis/snifferlog/players"

	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/orangeparis/ines/heartbeat"
	"bitbucket.org/orangeparis/ines/nping"
)

var (
	Version = "0.3"

	name = flag.String("name", "log", "name of the snifferlog (eg 0 , will publish to sniffer.0.>)")

	fPipe = flag.String("pipe", "", "linux pipe")
	fFile = flag.String("file", "", "rtlog file to read (eg: sample.rtlog)")

	nats   = flag.String("nats", "nats://127.0.0.1:4222", "nats server (eg nats://demo.nats.io:4222)")
	screen = flag.Bool("screen", false, "publish to screen")

	aggtime = flag.Int("aggregation-time", 15, "time in second to wait for a ticket ")

	helper = `read from a linux pipe OR a rtlog file and send messages to nats server OR screen
sample usage : snifferlog --name s1 --pipe /var/ines/pipe --nats nats://<host>:4222
options:
--name string
	name of the sniffer (default "0") (eg 0 , will publish to sniffer.0.>) 

--pipe string
    	linux pipe of rtlog lines
--file string
		rtlog file to read (eg: sample.rtlog)
		
--nats string
    	nats server (default "nats://127.0.0.1:4222")
--screen
    	publish to screen
`
)

func getPublisher() (pub publisher.Publisher, err error) {

	// return a publisher
	//  if --screen return a screen publisher
	//  else return a nats publisher

	// topic   eg sniffer.log  sniffer.log2
	var topic string

	//if strings.HasSuffix(*name, ".") {
	topic = fmt.Sprintf("sniffer.%s", *name)
	//} else {
	//	topic = fmt.Sprintf("sniffer.%s.", *name)
	//}

	if *screen == true {
		pub = publisher.NewScreenPublisher(topic)
		log.Println("snifferlog : set Publisher to screen")

	} else {
		// a nats publisher
		natsURL := *nats
		pub, err = publisher.NewNatsPublisher(topic, natsURL)
		log.Printf("snifferlog : set Publisher to nats publisher at %s", natsURL)

		// add a heartbeat emitter Heartbeat.sniffer.1
		full_name := "snifferlog." + *name
		e, _ := heartbeat.NewEmitter(full_name, natsURL, 10)
		e.Start()
		log.Printf("sniffer : started a heartbeat emitter [Heartbeat.%s] at  %s", full_name, natsURL)

		// handle call.
		c, err := nping.NewClient(*nats)
		if err != nil {
			log.Printf("Warning: failed to start handler for call.%s.ping\n", topic)
			return pub, err
		}
		defer c.Close()
		if err == nil {
			//pingTopic := "call." + topic + ".ping"
			sub, err := c.HandlePingService(topic)
			if err != nil {
				log.Printf("Warning: failed to subscribe to ping topic: %s\n", topic)
				return pub, err
			} else {
				defer sub.Unsubscribe()
				pingTopic := "call." + topic + ".ping"
				log.Printf("start handler for ping service at: %s\n", pingTopic)

			}

		}

	}
	log.Printf("snifferlog : set root topic to: %s\n", topic)

	return pub, err
}

func fileMode(pub publisher.Publisher, filename string) {

	//defer publisher.Close()

	player, err := players.NewFilePlayer(filename, pub, *aggtime)
	if err != nil {
		log.Printf("snifferlog : error %s\n", err.Error())
		log.Fatal("snifferlog : aborted.")
	}

	// launch the player loop
	log.Println("snifferlog : start player")
	player.Start()

	// flush publisher before quiting ??
	player.Close()

	//
	time.Sleep(10 * time.Second)

}

func pipeMode(pub publisher.Publisher, pipeName string) {

	player, err := players.NewPipePlayer(pipeName, pub, *aggtime)
	if err != nil {
		log.Printf("snifferlog : error: %s\n", err.Error())
		log.Fatal("sniffer : aborted.")
	}

	// launch the player loop
	log.Println("snifferlog : start player")
	player.Start()

}

func run() {

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, fmt.Sprintf("snifferlog %s\n", Version))
		fmt.Fprintf(os.Stderr, helper)
	}

	flag.Parse()

	// get a publisher corresponding to the flags ( screen or nats )
	publisher, err := getPublisher()
	if err != nil {
		log.Fatalf("snifferlog: cannot get a publisher: %s\n", err.Error())
		os.Exit(1)
	}

	// find  mode ( pipe or file )
	mode := "PIPE"
	if *fFile != "" {
		mode = "FILE"
	}

	//log.Printf("sniffer: starting")
	switch mode {
	case "FILE":
		// read from a rtlog file
		log.Printf("sniffer: create a file reader with %s\n", *fFile)
		fileMode(publisher, *fFile)

	default:
		// read from network interface (need to be root )
		log.Printf("snifferlog : create a pipereader with %s\n", *fPipe)
		pipeMode(publisher, *fPipe)
	}
	log.Printf("snifferlog : exiting")

}

func main() {
	run()
}
