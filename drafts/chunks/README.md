

a project PLAN B in case of overwhelmed in handling ticket


# idea

read ticket as they come , parse the them but dont publish them directly
create indexes on some values ( login , ??? )
instead we store them in "chunks" and store under a compress form

we only extract and publish tickets as we got subscriptions for them

    loop:
        create chunk
        for n = range chunk_size
            read next ticket
            parse it 
            create indexes
            compress it
            store it in current chunk
        close chunk
        
        goto loop
        
           
when whe receive a subscription for an element ( eg Login )

    seek in index to retrieve linked tickets in the chunks
    

index stores the coordinate of ticket eg ChunkIdentifier , Position (in the chunk )