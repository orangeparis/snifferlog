package chunks

type ChunkHeader struct {
	Size int // size in number of tickets

	Encoding int // identifer of the type of compression

	First float64 //  timestamp of the first ticket stored
	Last  float64 //  timestamp of the last ticket store

}

type ChunkElement struct {
	T       float64 // timestamp of the ticket
	Content []byte  // encoded content of the ticket

}

//
type Chunk []ChunkElement
