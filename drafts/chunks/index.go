package chunks

// the coordinate of a ticket chunk/position
type Coordinate struct {
	ChunkId  int64 // identifier of the chunk
	Position int64 // Position of ticket in the chunk
}

// an index and its ticket coordinates
type Index struct {
	Name        string
	Coordinates []Coordinate // coordinate list of all tickets linked to this index
}

type Indexes map[string]Index
