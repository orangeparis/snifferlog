package logdb

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	snifferlog "bitbucket.org/orangeparis/snifferlog/pkg"
	"log"
)

/*

	create a evs instance and store a sample file in it

*/

func draftStore(filename string, limit int) {

	source, err := sources.NewFileSource(filename, "")
	//source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		return
	}
	defer source.Close()

	// reading ticket loop
	n := 1
	for finished := false; finished == false; {
		data, err := source.Next()
		if err != nil {
			log.Printf("error reading Source %s : %s", source.Name(), err)
			finished = true
			break
		}

		ticket, err := snifferlog.NewTicket(data)
		if err != nil {
			log.Printf("Parser: failed to parse ticket (%s): %s\n", err.Error(), string(data))
			//p.PublishError(err.Error(), string(data))
			return
		}

		// store ticket in db
		_ = ticket

		n += 1
		if n > limit {
			println("limit encountered")
			break
		}
	}

	return

}
