package logdb

import (
	"bitbucket.org/orangeparis/ines/evs"
	"bitbucket.org/orangeparis/ines/evs/ulid"
	"context"
	"encoding/json"
	"github.com/dgraph-io/badger"
	"log"
	"testing"
	"time"
)

type SampleEvent struct {
	Number    int // we also add a number to keep track of the ground-truth creation order
	Timestamp time.Time
	ID        ulid.ULID
}

// create a slice of events (timestamps)
func sample_events(initial time.Time, n int, delta time.Duration) (events []evs.Event) {

	ts := initial
	for i := 1; i <= n; i++ {

		// create an event with no entropy ( timestamp ) and a blank data field
		evts, err := evs.NewTimestamp([]byte{}, ts)
		if err != nil {
			log.Fatal("cannot compute ulid from timestamp")
			return
		}
		ulid, _ := evts.GetUlid()
		payload := SampleEvent{i, ts, ulid}

		// serialize the event payload (to JSON for simplicity)
		data, err := json.Marshal(payload)
		if err != nil {
			log.Fatal("cannot serialize to json")
		}
		evts.Data = data

		//ev, _ := evs.NewTimestamp(data, ts)
		//ev := evs.StoredEvent{Timestamp: ts, Data: data}

		events = append(events, evts)

		// increment ts
		ts = ts.Add(delta)

	}
	firstEvent, _ := events[0].GetTimestamp()
	lastEvent, _ := events[len(events)-1].GetTimestamp()
	timeRange := lastEvent.Sub(firstEvent)
	log.Printf(" time range of %d items is %s", n, timeRange)
	return events
}

func TestEvs(t *testing.T) {

	// create a store
	tmpDir := "./badgerdb_tmp"
	opts := badger.DefaultOptions(tmpDir)
	//opts.Dir = tmpDir
	//opts.ValueDir = tmpDir
	db, _ := badger.Open(opts)

	err := db.DropAll()
	//store := &evs.Store{DB: db}

	store, _ := evs.NewStore(db, "dummy", 60*time.Second)
	defer store.Close()

	// create a event
	data := `{"key":"value"}`
	ts0 := time.Unix(1000000, 0)

	event, err := evs.NewTimestamp([]byte(data), ts0)

	var events []evs.Event
	events = append(events, event)

	// write events to db
	err = store.StoreEvent(events...)
	if err != nil {
		log.Printf("cannot create events in database")
		t.Fail()
	}

	// get the event ID ( with no entropy ) as a base start for query
	eventID := event.ID

	time.Sleep(2 * time.Second)

	// read events
	_ = eventID
	start := ts0
	delay, _ := time.ParseDuration("500ms")

	b0, b1, err := evs.UlidTimeBoundaries(start, delay)
	if err != nil {
		t.Fail()
		return
	}

	// try to read keys if any
	keys, err := store.WindowKeys(start, delay)
	log.Printf("keys collected within window: %d\n", len(keys))

	ctx := context.Background()
	eventChan, err := store.WindowRead(ctx, b0.String(), b1.String())

	if err != nil {
		log.Printf("WindowRead failed:%s", err.Error())
		t.Fail()
		return
	}

	var collected []evs.Event
	var info map[string]interface{}

	for ev := range eventChan {
		valid, reason := ev.IsValid()
		if valid {
			collected = append(collected, ev)
			err = json.Unmarshal(ev.Data, &info)
			if err == nil {
				println("event: ", ev.ID, info["key"].(string))
			}
		} else {
			println("error event :", reason)
			break
		}
	}

	if len(collected) > 0 {
		println("collected : ", len(collected))
		//e1 = collected[0]
		//t1, _ = e1.GetTimestamp()
		//log.Printf("first ulid key : t= %s -> %s\n", e1.ID, t1)
		//
		//e2 = collected[len(collected)-1]
		//t2, _ = e2.GetTimestamp()
		//log.Printf("last  ulid key : t= %s -> %s\n", e2.ID, t2)

	} else {
		println("no event collected")
	}

}

func TestEvsOnSampleEvents(t *testing.T) {

	// create a store
	tmpDir := "./badgerdb_tmp"
	opts := badger.DefaultOptions(tmpDir)
	//opts.Dir = tmpDir
	//opts.ValueDir = tmpDir
	db, err := badger.Open(opts)
	if err != nil {
		t.Fail()
		return
	}
	db.DropAll()

	store, _ := evs.NewStore(db, "dummy", 60*time.Second)
	defer store.Close()

	// create sample events
	var nbEvents = 10
	ts0 := time.Unix(1000000, 0)
	delta, _ := time.ParseDuration("1s")
	events := sample_events(ts0, nbEvents, delta)
	log.Printf("create %d events, starting at %s\n", nbEvents, ts0)

	// write events to db
	err = store.StoreEvent(events...)
	if err != nil {
		log.Printf("cannot create events in database")
		t.Fail()
	}

	// queries

	var p1, p2 string
	var t1, t2 time.Time

	//
	// select keys from a time range within window
	//
	start := ts0.Add(5 * time.Millisecond) // we start just after the first reccord
	delay, _ := time.ParseDuration("10s")
	expected_key_number := 9

	keys, err := store.WindowKeys(start, delay)
	log.Printf("keys collected within window: %d\n", len(keys))

	if len(keys) != expected_key_number {
		log.Printf("we should have %d keys, we got: %d\n", expected_key_number, len(keys))
	}

	if len(keys) > 0 {

		p1 = keys[0]
		t1, _ = evs.TimestampFromUlid(p1)
		log.Printf("first ulid key : t= %s -> %s\n", p1, t1)

		p2 = keys[len(keys)-1]
		t2, _ = evs.TimestampFromUlid(p2)
		log.Printf("last  ulid key : t= %s -> %s\n", p2, t2)

	}

}

func TestStoreSample(t *testing.T) {

	filename1 := "../../samples/LogFonct.log-20191005"
	// filename2 := "../samples/LogFonct.log-20191006"

	draftStore(filename1, 10)

	println("Done.")
}

func TestEvsExperimentalRead(t *testing.T) {

	// create a store
	tmpDir := "./badgerdb_tmp"
	opts := badger.DefaultOptions(tmpDir)
	//opts.Dir = tmpDir
	//opts.ValueDir = tmpDir
	db, err := badger.Open(opts)
	if err != nil {
		t.Fail()
		return
	}
	db.DropAll()

	store, _ := evs.NewStore(db, "dummy", 60*time.Second)
	defer store.Close()

	// create sample events
	var nbEvents = 10
	ts0 := time.Unix(1000000, 0)
	delta, _ := time.ParseDuration("1s")
	events := sample_events(ts0, nbEvents, delta)
	log.Printf("create %d events, starting at %s\n", nbEvents, ts0)

	// write events to db
	err = store.StoreEvent(events...)
	if err != nil {
		log.Printf("cannot create events in database")
		t.Fail()
	}

	// queries

	//
	// select query range
	//
	start := ts0.Add(5 * time.Millisecond) // we start just after the first record
	delay, _ := time.ParseDuration("3s")   // we stop after 2 second
	expectedKeyNumber := 3

	// try to read keys if any
	ctx := context.Background()
	eventChan, err := store.ExperimentalRead(ctx, start, delay)

	if err != nil {
		log.Printf("WindowRead failed:%s", err.Error())
		t.Fail()
		return
	}

	var collected []evs.Event
	var info map[string]interface{}

	for ev := range eventChan {
		valid, reason := ev.IsValid()
		if valid {
			collected = append(collected, ev)
			err = json.Unmarshal(ev.Data, &info)
			if err == nil {
				println("event: ", ev.ID, int(info["Number"].(float64)))
			}
		} else {
			println("error event :", reason)
			break
		}
	}
	close(eventChan)

	if len(collected) != expectedKeyNumber {
		log.Printf("we should have %d keys, we got: %d\n", expectedKeyNumber, len(collected))
	}

	return

}
