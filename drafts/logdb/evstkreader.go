package logdb

import (
	"bitbucket.org/orangeparis/ines/evs"
	snifferlog "bitbucket.org/orangeparis/snifferlog/pkg"
	"context"
	"encoding/json"
	"errors"
	"log"
	"time"
)

type TicketReader interface {
	Name() string
	Next() (*snifferlog.TicketInes, error)
	Close()
}

type EvsTicketReader struct {
	name  string
	Store *evs.Store

	Start    time.Ticker
	Duration time.Duration

	eventChannel chan evs.Event
}

func NewEvsTicketReader(name string, store *evs.Store) (r *EvsTicketReader, err error) {

	r = &EvsTicketReader{name: name, Store: store}
	return

}

// implements ticker reader ( blocking read
func (r *EvsTicketReader) Next() (ticket *snifferlog.TicketInes, err error) {

	select {
	case e := <-r.eventChannel:

		//ticket = &snifferlog.TicketInes{}
		if v, msg := e.IsValid(); v == true {
			err := json.Unmarshal(e.Data, &ticket)
			if err != nil {
				log.Printf("%s\n", err.Error())
				return ticket, err
			}
		} else {
			// not a valid event
			err := errors.New(msg)
			return ticket, err
		}
		return ticket, nil

	}

	return
}
func (r *EvsTicketReader) Close() {
	if r.eventChannel != nil {
		close(r.eventChannel)
	}
}

//
// private function
//

func (r *EvsTicketReader) Run(ctx context.Context, start time.Time, duration time.Duration) error {

	ch, err := r.Store.ExperimentalRead(ctx, start, duration)
	if err != nil {
		return err
	}
	r.eventChannel = ch
	return nil
}
