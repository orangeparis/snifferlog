package logdb

import (
	"bitbucket.org/orangeparis/ines/evs"
	"context"
	"fmt"
	"github.com/dgraph-io/badger"
	"testing"
	"time"
)

func TestEvsTkReader(t *testing.T) {

	tmpDir := "./badgerdb_tmp"
	starter := "2019-10-05T01:00:52.431+02:00"
	duration := "1s"

	// create a store
	opts := badger.DefaultOptions(tmpDir)
	//opts.Dir = tmpDir
	//opts.ValueDir = tmpDir
	db, err := badger.Open(opts)
	if err != nil {
		t.Fail()
	}
	defer db.Close()

	store, _ := evs.NewStore(db, "dummy", 6*time.Hour)
	defer store.Close()

	r, err := NewEvsTicketReader("reader", store)
	if err != nil {
		t.Fail()
		return
	}

	t0, err := time.Parse(TIMESTAMP_LAYOUT, starter)
	if err != nil {
		t.Fail()
		return
	}
	d, err := time.ParseDuration(duration)
	if err != nil {
		t.Fail()
		return
	}

	ctx := context.Background()
	r.Run(ctx, t0, d)

	for {

		x, err := r.Next()
		if err != nil {
			r.Close()
			break
		}
		// fmt.Printf("%s\n" ,x.Tags )
		if x != nil {
			fmt.Printf("%s\n", x.Header.TimeStamp)
		} else {
			println()
		}
	}

	println("Done")

}
