package logdb

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	"bitbucket.org/orangeparis/ines/evs"
	snifferlog "bitbucket.org/orangeparis/snifferlog/pkg"
	"encoding/json"
	"log"
	"time"
)

/*

	create a evs instance and store a sample file in it

*/

const (
	TIMESTAMP_LAYOUT = time.RFC3339
)

type EvsTicketWriter struct {
	Source sources.DataSource
	Store  *evs.Store
}

func (w *EvsTicketWriter) Run() {

	source := w.Source

	// reading ticket loop
	//n := 1
	for finished := false; finished == false; {

		// start a chunk
		var events []evs.Event
		for i := 1; i < 100; {

			data, err := source.Next()
			if err != nil {
				if err.Error() == "EOF" {
					log.Printf("EOF Source: %s\n", source.Name())
				} else {
					log.Printf("error reading Source %s : %s", source.Name(), err)
				}
				finished = true
				break
			}

			ticket, err := snifferlog.NewTicket(data)
			if err != nil {
				log.Printf("Parser: failed to parse ticket (%s): %s\n", err.Error(), string(data))
				//p.PublishError(err.Error(), string(data))
				return
			}

			payload, err := json.Marshal(ticket)

			// make an evs event from the ticket
			tp := ticket.Header.TimeStamp
			_ = tp
			ts, err := time.Parse(TIMESTAMP_LAYOUT, tp)
			if err != nil {
				log.Printf("Timestamp converting error: %s\n", err.Error())
				continue
			}

			event, err := evs.NewTimestamp(payload, ts)
			events = append(events, event)

			i++

		}

		// store events chunk
		err := w.Store.StoreEvent(events...)
		if err != nil {
			log.Printf("cannot create events in database")

		}

	}

	return

}
