package logdb

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	"bitbucket.org/orangeparis/ines/evs"
	"github.com/dgraph-io/badger"
	"log"
	"testing"
	"time"
)

func TestEvsTkWriter(t *testing.T) {

	tmpDir := "./badgerdb_tmp"
	filename := "../../samples/LogFonct.log-20191005"

	source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		return
	}
	defer source.Close()

	// create a store
	opts := badger.DefaultOptions(tmpDir)
	//opts.Dir = tmpDir
	//opts.ValueDir = tmpDir
	db, _ := badger.Open(opts)
	if err != nil {
		t.Fail()
	}
	defer db.Close()
	db.DropAll()

	store, _ := evs.NewStore(db, "dummy", 6*time.Hour)
	defer store.Close()

	tkw := &EvsTicketWriter{Source: source, Store: store}

	tkw.Run()

	println("Done.")

}
