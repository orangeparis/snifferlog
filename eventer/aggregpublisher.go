package eventer

import (
	//"bitbucket.org/orangeparis/ines/datasource"
	"bitbucket.org/orangeparis/ines/msgsource"
	"bitbucket.org/orangeparis/snifferlog/agregator"
	"bitbucket.org/orangeparis/snifferlog/players"
	"context"
	"encoding/json"
	"log"
	"time"
)

/*

	MsgPublisher

	a publisher which is also a MsgSource

	read data from a data source (


*/

type AggregatorPublisher struct {
	Secondary *msgsource.DummyChannelSource
}

func NewAggrgatorPublisher() (p *AggregatorPublisher) {

	s := msgsource.NewDummyChannelSource("ChannelSource", 2048)

	p = &AggregatorPublisher{Secondary: s}

	return p
}

// implements Publisher interface
func (m *AggregatorPublisher) Publish(topic string, message []byte) (err error) {

	topic = "sniffer.log." + topic
	m.Secondary.Add(topic, message)
	return

}

func run2() (err error) {

	filename := "../samples/LogFonct.log-20191005"

	// create msg publisher
	pub := NewAggrgatorPublisher()

	// create a file player
	pl, err := players.NewFilePlayer(filename, pub)
	if err != nil {
		return err
	}

	// start the player
	go pl.Start()

	// read messages from msg
	agMap := agregator.NewAggregatorMap(5 * time.Second)
	go agMap.Run(context.Background())

	for m := range pub.Secondary.Channel() {

		//m.Display()

		data := make(map[string]string)
		err := json.Unmarshal(m.Data, &data)

		body, err := m.Unmarshal()
		if err != nil {
			log.Printf("%s\n", err.Error())
		}
		id, ok := body["TraceID"]
		if ok == false {
			log.Printf("NO TRACEID")
			continue
		}

		agMap.Feed(id.(string), data)

		//println("*v\n", body)

	}

	return

}
