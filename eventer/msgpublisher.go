package eventer

import (
	//"bitbucket.org/orangeparis/ines/datasource"
	"bitbucket.org/orangeparis/ines/msgsource"
	"bitbucket.org/orangeparis/snifferlog/players"
	"log"
	"strings"
)

/*

	MsgPublisher

	a publisher which is also MsgSource

	read data from a data source (


*/

type MsgPublisher struct {
	Secondary *msgsource.DummyChannelSource
}

func NewMsgPublisher() (p *MsgPublisher) {

	s := msgsource.NewDummyChannelSource("ChannelSource", 2048)

	p = &MsgPublisher{Secondary: s}

	return p
}

// implements Publisher interface
func (m *MsgPublisher) Publish(topic string, message []byte) (err error) {

	topic = "sniffer.log." + topic
	m.Secondary.Add(topic, message)
	return

}

func run() (err error) {

	filename := "../samples/LogFonct.log-20191005"

	// create msg publisher
	pub := NewMsgPublisher()

	// create a file player
	pl, err := players.NewFilePlayer(filename, pub, 15)
	if err != nil {
		return err
	}

	// start the player
	go pl.Start()

	// read messages from msg

	for m := range pub.Secondary.Channel() {

		m.Display()

		// subjects sniffer.log.ticket.fti/pgc69g6 , , sniffer.log.mq
		sparts := strings.Split(m.Subject, ".")
		_ = sparts

		decode := false

		if len(sparts) >= 3 {
			t := sparts[2]
			if t == "client" {
				decode = true
				//m.Display()
			}
		}

		// body
		if decode {
			body, err := m.Unmarshal()
			if err != nil {
				log.Printf("%s\n", err.Error())
			}
			println("*v\n", body)
		}

		// "TraceID" Accounting_Subscriber_Start

	}

	return

}
