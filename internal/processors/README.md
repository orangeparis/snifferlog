
processors treats message logs to extract info




benchmark for ticket file of 1.09 Gb

=== RUN   TestDecodeTicket
2019/11/14 18:30:14 error reading file ../../samples/LogFonct.log-20191005 : EOF
decode [2840858] lines in 9.224303293s--- PASS: TestDecodeTicket (9.22s)
PASS


=== RUN   TestDecodeTicketInes
2019/11/14 19:07:47 error reading file ../../samples/LogFonct.log-20191005 : EOF
decode [2840858] lines in 11.998028021s--- PASS: TestDecodeTicketInes (12.00s)
PASS

some stats


=== RUN   TestStatsOnTicketInes
2019/11/15 13:43:00 error reading file ../../samples/LogFonct.log-20191005 : EOF
decode [2840858] lines in 12.253067376s
stats: 
map[InformationalSystem:
    map[
        Gilda ACK:218 
        Gilda TKT:88 
        Meteor TKT:1419304] 
    LogType:map[
        FUNCT_CLIENT:1940 
        FUNCT_TICKET:1419610 
        FUNCT_TICKET_MQ:1419308] 
    MessType:map[
        Accounting Host IPv4 Interim:801 
        Accounting Host IPv4 Start:320 
        Accounting Host IPv4 Stop:307162 
        Accounting Host IPv6 Interim:729 
        Accounting Host IPv6 Start:320 
        Accounting Host IPv6 Stop:180645 
        Accounting Subscriber Interim:729 
        Accounting Subscriber Start:320 
        Accounting Subscriber Stop:930524 
        MQ Series Ticket:1419308]]
--- PASS: TestStatsOnTicketInes (12.25s)
PASS


=== RUN   TestStatsOnTicketInes
2019/11/15 13:46:33 error reading file ../../samples/LogFonct.log-20191006 : EOF
decode [2934171] lines in 12.687110618s
stats: 
    map[
        InformationalSystem:map[
            Gilda ACK:197 
            Gilda TKT:80
            Meteor TKT:1466044]
        LogType:map[
            FUNCT_CLIENT:1804 
            FUNCT_TICKET:1466321 
            FUNCT_TICKET_MQ:1466046] 
        MessType:map[
            Accounting Host IPv4 Interim:735 
            Accounting Host IPv4 Start:296 
            Accounting Host IPv4 Stop:269897 
            Accounting Host IPv6 Interim:666 
            Accounting Host IPv6 Start:296 
            Accounting Host IPv6 Stop:630839 
            Accounting Subscriber Interim:666 
            Accounting Subscriber Start:296 
            Accounting Subscriber Stop:564434 
            MQ Series Ticket:1466046]]
        ClientOption:map[
            :1764 
            FTTH:30 
            VDSL2D:10
            
--- PASS: TestStatsOnTicketInes (12.69s)
PASS



=== RUN   TestStatsOnTicketInes
2019/11/15 14:05:57 error reading file ../../samples/LogFonct.log-20191006 : EOF
decode [2934171] lines in 12.044910629s
stats: map[
    ClientOption:map[
        :1764 
        FTTH:30 
        VDSL2D:10] 
    InformationalSystem:map[
        Gilda ACK:197 
        Gilda TKT:80 
        Meteor TKT:1466044] 
    LogType:map[
        FUNCT_CLIENT:1804 
        FUNCT_TICKET:1466321 
        FUNCT_TICKET_MQ:1466046] 
    MessType:map[
        Accounting Host IPv4 Interim:735 
        Accounting Host IPv4 Start:296 
        Accounting Host IPv4 Stop:269897 
        Accounting Host IPv6 Interim:666 
        Accounting Host IPv6 Start:296 
        Accounting Host IPv6 Stop:630839 
        Accounting Subscriber Interim:666 
        Accounting Subscriber Start:296 
        Accounting Subscriber Stop:564434 
        MQ Series Ticket:1466046]]

--- PASS: TestStatsOnTicketInes (12.05s)
PASS



//
// test with nilPublisher to benchmark parse + serialize ( 3M ticket in 30s )
//                                          100 000 ticket/sec
//
=== RUN   TestProcessorInes
2019/11/16 14:12:22 error reading source FileSource(../../samples/LogFonct.log-20191005) : EOF
--- PASS: TestProcessorInes (31.50s)
PASS
