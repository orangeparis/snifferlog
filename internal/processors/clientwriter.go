package processors

import (
	//"bitbucket.org/orangeparis/snifferlog/internal/filters"
	filters "bitbucket.org/orangeparis/ines/tagfilter"
	//"bitbucket.org/orangeparis/snifferlog/internal/publishers"
	publishers "bitbucket.org/orangeparis/ines/publisher"

	"encoding/json"
	"log"
)

/*
	client writer select messages with

		Tag LogType == "FUNCT_CLIENT"

	and publish messages

		topic: sniffer.rtlog.client
		payload :
			"AcctProfile":"NO_PROFILE",
			"Address":"",
			"AllocationMode":"",
			"BNGID":"NESONDE",
			"BNGIP":"172.20.210.253",
			"CircuitID":"UNKNOWN",
			"ClientAccessMode":"3",
			"ClientOption":"",
			"DeviceID":"",
			"ErrCode":"7",
			"ErrMess":"Connection not found in Database",
			"EventTimeStamp":"2019-10-05 01:00:50.000",
			"LineID":"00000249207802",
			"LogType":"FUNCT_CLIENT",
			"Login":"",
			"MessType":"Accounting Subscriber Interim",
			"ProcTime":"7",
			"QoSProfile":"NO_PROFILE",
			"StateInd":"2",
			"TimeStamp":"2019-10-05 01:00:52.424",
			"TraceID":"TID6ab9e2e333e615cb02664562a98ef"}



*/

type ClientWriter struct {
	publisher publishers.Publisher
	filters   []filters.TagFilter
}

// implements Writer interface
func (w ClientWriter) Feed(ticket TicketInes) (match bool, err error) {

	// apply filters
	for _, filter := range w.filters {
		if filter != nil {
			ok := filter.Filter(ticket.Tags)
			if !ok {
				// dont match the filter : skip the ticket
				return false, nil
			}
		}
	}

	// build and send messages
	body, err := json.Marshal(ticket.Tags)
	if err != nil {
		log.Printf("ClientWriter: cannot marshal ticket:%s\n", err)
		return true, err
	}
	_ = w.publisher.Publish("client", body)
	return true, nil

}

func NewClientWriter(publisher publishers.Publisher) ClientWriter {

	f0 := filters.NewTagListFilter("LogType", "FUNCT_CLIENT")

	x := ClientWriter{publisher: publisher}
	x.filters = append(x.filters, f0)

	return x

}
