package processors

import (
	//"bitbucket.org/orangeparis/snifferlog/internal/filters"
	filters "bitbucket.org/orangeparis/ines/tagfilter"
	// "bitbucket.org/orangeparis/snifferlog/internal/publishers"
	publishers "bitbucket.org/orangeparis/ines/publisher"
	"encoding/json"
	"log"
)

/*
	meteor writer select messages with

		Tag LogType == "FUNCT_TICKET"
		//Tag InformationalSystem == "Meteor TKT"
		//Tag ErrCode == "0"

	and publish messages

		topic: sniffer.rtlog.ticket
		payload :
			MessType="Accounting Subscriber Stop"
			StateInd="0" ErrCode="0"
			ErrMess="Operation successful"
			InformationalSystem="Meteor TKT"
			Serial="9302f16d01-6d50-00fb-9656-77c0a8d318-351"
			Login="fti/k6qx43b"
			LineID="00000898286000"
			CircuitID="UNKNOWN"
			BNGID="NEMET015"
			BNGIP="10.15.146.245"
			Address=""
			EventCause="6"

*/

// TicketWriter
type MeteorWriter struct {
	publisher publishers.Publisher
	filters   []filters.TagFilter
}

// implements Writer interface
func (w MeteorWriter) Feed(ticket TicketInes) (match bool, err error) {

	// apply filters
	for _, filter := range w.filters {
		if filter != nil {
			ok := filter.Filter(ticket.Tags)
			if !ok {
				// dont match the filter : skip the ticket
				return false, nil
			}
		}
	}

	// build and send messages
	body, err := json.Marshal(ticket.Tags)
	if err != nil {
		log.Printf("ProcessorInes: cannot marshal ticket:%s\n", err)
		return true, err
	}
	subject := "ticket"
	login, ok := ticket.Tags["Login"]
	if ok == true {
		if login != "" {
			subject = "ticket." + login
		} else {
			subject = "ticket.unknown"
		}

	}

	_ = w.publisher.Publish(subject, body)
	return true, nil
}

func NewMeteorWriter(publisher publishers.Publisher) MeteorWriter {

	f0 := filters.NewTagListFilter("LogType", "FUNCT_TICKET")
	//f1 := filters.NewTagFilter("InformationalSystem", "Meteor TKT")
	//f2 := filters.NewTagFilter("ErrCode", "0")

	x := MeteorWriter{publisher: publisher}
	x.filters = append(x.filters, f0)
	//x.filters = append(x.filters, f1)
	//x.filters = append(x.filters, f2)

	return x

}
