package processors

import (
	//"bitbucket.org/orangeparis/snifferlog/internal/filters"
	filters "bitbucket.org/orangeparis/ines/tagfilter"
	//"bitbucket.org/orangeparis/snifferlog/internal/publishers"
	publishers "bitbucket.org/orangeparis/ines/publisher"
	"encoding/json"
	"log"
)

/*
	mq writer select messages with

		Tag LogType == "FUNCT_TICKET_MQ"
		//Tag ErrCode == "0"

	and publish messages

		topic: sniffer.rtlog.mq
		payload :
			TimeStamp="2019-10-06 03:33:29.558"
			TraceID="TID684e1ca0a546e3465cf47b7158717"
			LogType="FUNCT_TICKET_MQ"
			MessType="MQ Series Ticket"
			StateInd="0"
			ErrCode="0"
			ErrMess="Operation successful"
			MQSerial="TID9eb33c5c01-6d50-00ee-f6e8-c9c0a8d318"
			TicketNBR="1"


*/

type MqWriter struct {
	publisher publishers.Publisher
	filters   []filters.TagFilter
}

// implements Writer interface
func (w MqWriter) Feed(ticket TicketInes) (match bool, err error) {

	// apply filters
	for _, filter := range w.filters {
		if filter != nil {
			ok := filter.Filter(ticket.Tags)
			if !ok {
				// dont match the filter : skip the ticket
				return false, nil
			}
		}
	}

	// build and send messages
	body, err := json.Marshal(ticket.Tags)
	if err != nil {
		log.Printf("ProcessorInes: cannot marshal ticket:%s\n", err)
		return true, err
	}
	_ = w.publisher.Publish("mq", body)
	return true, nil
}

func NewMqWriter(publisher publishers.Publisher) MqWriter {

	f0 := filters.NewTagListFilter("LogType", "FUNCT_TICKET_MQ")
	//f1 := filters.NewTagFilter("ErrCode", "0")

	x := MqWriter{publisher: publisher}
	x.filters = append(x.filters, f0)
	//x.filters = append(x.filters, f1)

	return x

}
