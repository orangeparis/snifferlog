package processors

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	//"bitbucket.org/orangeparis/snifferlog/pipes"
	"fmt"
	"log"
)

/*

	parser

	datasource (tickets)   --> parser  --> aggregator


*/

func Parse(source sources.DataSource, out pipes.ScreenTagPipeOut) {

	n := 0
	for finished := false; finished == false; {
		data, err := source.Next()
		if err != nil {
			log.Printf("error reading Source %s : %s", source.Name(), err)
			finished = true
			break
		}

		ticket, err := NewTicket(data)
		if err != nil {
			log.Printf("Parser: failed to parse ticket (%s): %s\n", err.Error(), string(data))
			//p.PublishError(err.Error(), string(data))
			return
		}

		n += 1
		_ = ticket
		out.Feed("topic", ticket.Tags)
		//fmt.Printf("%s\n",ticket.Tags)

	}

	fmt.Printf("tickets: %d\n", n)
}
