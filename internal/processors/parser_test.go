package processors

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	"bitbucket.org/orangeparis/snifferlog/pipes"
	"log"
	"testing"
)

func TestParser(t *testing.T) {

	filename := "../../samples/LogFonct.log-20191005"

	source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		t.Fail()
		return
	}
	dest := pipes.ScreenTagPipeOut{}

	Parse(source, dest)

	println("done")

}
