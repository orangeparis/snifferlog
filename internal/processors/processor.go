package processors

import (
	// "bitbucket.org/orangeparis/snifferlog/internal/filters"
	filters "bitbucket.org/orangeparis/ines/tagfilter"
	"fmt"

	//"bitbucket.org/orangeparis/snifferlog/internal/publishers"
	publishers "bitbucket.org/orangeparis/ines/publisher"
	//"bitbucket.org/orangeparis/snifferlog/internal/sources"
	sources "bitbucket.org/orangeparis/ines/datasource"
	"context"
	"log"
)

/*
	process a ticket Source  ( from file or pipe )
	apply filters
	use publisher


*/

type ProcessorInes struct {
	Source    sources.DataSource   // a Source ( either file or pipe )
	publisher publishers.Publisher // a publisher either nats or screen
	filters   []filters.TagFilter  // a set of global filters

	writers []Writer // a set of writers

}

func NewProcessor(source sources.DataSource, publisher publishers.Publisher, filter ...filters.TagFilter) (p *ProcessorInes, err error) {

	if filter == nil {
		filter = make([]filters.TagFilter, 0, 0)
	}
	p = &ProcessorInes{Source: source, publisher: publisher, filters: filter}

	return p, nil
}

func (p *ProcessorInes) AddWriter(writer Writer) {
	p.writers = append(p.writers, writer)
}

func (p *ProcessorInes) Run(ctx context.Context) {

	for finished := false; finished == false; {
		data, err := p.Source.Next()
		if err != nil {
			log.Printf("error reading Source %s : %s", p.Source.Name(), err)
			finished = true
			break
		}
		p.RunOnce(data)

	}
}

func (p *ProcessorInes) RunOnce(data []byte) {

	ticket, err := NewTicket(data)
	if err != nil {
		log.Printf("ProcessorInes: failed to parse ticket (%s): %s\n", err.Error(), string(data))
		p.PublishError(err.Error(), string(data))
		return
	}

	// apply global filters
	for _, filter := range p.filters {
		if filter != nil {
			ok := filter.Filter(ticket.Tags)
			if !ok {
				// dont match the filter : skip the ticket
				return
			}
		}
	}

	// dispatch to writers
	var match bool

	for _, writer := range p.writers {
		if writer != nil {
			match, err = writer.Feed(ticket)
			_ = err
			if match {
				// the current writer has found a match : skip the other writers
				break
			}
		}
	}
	if match == false {
		// no filters found
		p.PublishError("NO FILTER MATCH", string(data))
	}
}

func (p *ProcessorInes) PublishError(code string, message string) {
	msg := fmt.Sprintf(`{"error":"%s","message":"%s"`, code, message)
	_ = p.publisher.Publish("error", []byte(msg))
}
