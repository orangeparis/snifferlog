package processors_test

import (
	"bitbucket.org/orangeparis/snifferlog/internal/processors"
	//"bitbucket.org/orangeparis/snifferlog/internal/publishers"
	publishers "bitbucket.org/orangeparis/ines/publisher"
	//"bitbucket.org/orangeparis/snifferlog/internal/sources"
	sources "bitbucket.org/orangeparis/ines/datasource"

	"context"
	"log"
	"testing"
)

func TestProcessorInes(t *testing.T) {

	filename := "../../samples/LogFonct.log-20191005"

	//publisher := publishers.NewScreenPublisher("sniffer.log")
	publisher := publishers.NewNilPublisher()

	source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		t.Fail()
		return
	}

	////filter1 := filters.LogTypeFilter{ LogTypes: []string{"FUNCT_CLIENT"}}
	//filter1 := filters.NewInformationalSystemFilter("Meteor TKT")
	//filter2 := filters.TagFilter{Tag: "ErrCode", Values: []string{"0"}}
	p, _ := processors.NewProcessor(source, publisher, nil)

	// add writers
	p.AddWriter(processors.NewMeteorWriter(publisher))
	p.AddWriter(processors.NewClientWriter(publisher))
	p.AddWriter(processors.NewMqWriter(publisher))

	ctx := context.Background()
	p.Run(ctx)

}
