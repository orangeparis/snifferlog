package processors

import (
	"encoding/json"
	"errors"
	"strings"
)

/*

	a parser for rtlog ticket:  timestamp origin INES: (key="value" )*

	sample ticket:

2019-10-05T01:00:52.431+02:00 innapnoi3fed06 INES: TimeStamp="2019-10-05 01:00:52.424" EventTimeStamp="2019-10-05 01:00:50.000" TraceID="TID6ab9e2e333e615cb02664562a98ef" LogType="FUNCT_CLIENT" MessType="Accounting Subscriber Interim" ProcTime="7" StateInd="2" ErrCode="7" ErrMess="Connection not found in Database" ClientAccessMode="3" Login="" DeviceID="" Address="" AllocationMode="" BNGID="NESONDE" BNGIP="172.20.210.253" AcctProfile="NO_PROFILE" QoSProfile="NO_PROFILE" LineID="00000249207802" CircuitID="UNKNOWN" ClientOption=""





*/

type TicketHeader struct {
	TimeStamp string // timestamp of the trace  eg   2019-10-05T01:00:53.722+02:00
	Origin    string // machine originating the log eg : innapnoi3bed02
	Marker    string // mark the start of key/values  eg  'INES:'
	// 2019-10-05T01:00:53.722+02:00 innapnoi3bed02 INES:
}

// ticket ines , format:  timestamp origin INES: (key="value" )*
type TicketInes struct {
	Line   string
	Header *TicketHeader
	Tags   map[string]string
	Err    error
	cursor int
}

// create an rtlog ticket from data
func NewTicket(data []byte) (ticket TicketInes, err error) {

	//header := &TicketHeader{}
	//tags := make(map[string]string)

	ticket = TicketInes{Line: string(data), Header: &TicketHeader{}, Tags: make(map[string]string)}

	err = ticket.Parse()
	return ticket, err
}

func (t TicketInes) Parse() (err error) {

	err = t.ParseHeader()
	if err != nil {
		return err
	}

	err = t.ParseTags()
	if err != nil {
		return err
	}
	return nil
}

//
func (t *TicketInes) ParseHeader() error {

	text := t.Line
	header := t.Header
	state := 0
	b0 := 0

forloop:
	for i := t.cursor; i < len(t.Line); i++ {
		switch state {

		case 0:
			// process Timestamp
			if text[i] == ' ' {
				// extract timestamp
				header.TimeStamp = text[b0:i]
				b0 = i
				state = 1
			}
		case 1:
			// process log originator
			if text[i] == ' ' {
				// extract origin
				header.Origin = text[b0+1 : i]
				b0 = i
				state = 2
			}

		case 2:
			// process marker
			if text[i] == ' ' {
				// extract marker
				header.Marker = text[b0+1 : i]
				t.cursor = i + 1
				break forloop
			}
		}
	}
	if t.Header.Marker != "INES:" {
		err := errors.New("Bad Header")
		return err
	}
	return nil
}

func (t *TicketInes) ParseTags() error {

	//data := make(map[string]string)

	text := t.Line

	var state = 0

	var k0 = t.cursor
	var k1 = t.cursor
	var v0 = t.cursor
	var v1 = t.cursor

	var lastKey = ""

	state = 1
	for i := t.cursor; i < len(text); i++ {

		switch state {

		case 0:
			// process blank separating 2 kv entries
			if text[i] == ' ' {
				k0 = i
				k1 = i
				v0 = 1
				v1 = 1
				state = 1
			}

		case 1:
			// process key  [a-zA-Z0-9]+
			if text[i] == '=' {
				// we detect end of key : remember end 0f key k1
				k1 = i
				lastKey = text[k0:k1]
				//fmt.Printf("key: %s\n", text[k0:k1])
				//out = append(out, text[k0:k1])
				state = 2
			}

		case 2:
			//  process  "
			if text[i] == '"' {
				// we detect start of value
				v0 = i + 1
				v1 = v0
				state = 3
			}
		case 3:
			// process value
			if text[i] == '"' {
				// we detect the end of value
				v1 = i
				t.Tags[strings.TrimSpace(lastKey)] = text[v0:v1]
				//fmt.Printf("value: %s\n", text[v0:v1])
				//out = append(out, text[v0:v1])
				state = 0
			}
		}
	}
	if state != 0 {
		// bad key value format
		err := errors.New("bad key/value format")
		return err
	}
	return nil

}

// tojson : json representation of a ticket
func (t *TicketInes) ToJson() (data []byte, err error) {

	// build and send messages
	data, err = json.Marshal(t.Tags)
	if err != nil {
		//log.Printf("TicketInes: cannot marshal ticket:%s\n", err)
		return data, err
	}
	return data, err
}
