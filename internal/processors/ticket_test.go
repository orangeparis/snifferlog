package processors_test

import (
	"bitbucket.org/orangeparis/snifferlog/internal/processors"
	//"bitbucket.org/orangeparis/snifferlog/internal/sources"
	sources "bitbucket.org/orangeparis/ines/datasource"

	"fmt"
	"log"
	"testing"
	"time"
)

func TestDecodeTicketInes(t *testing.T) {

	//filename := "../../test/samples/rtlog_10.log"
	filename := "../../samples/LogFonct.log-20191005"

	start := time.Now()

	s, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Printf("%s", err.Error())
		t.Fail()
		return
	}

	count := 0
	for finished := false; finished == false; {
		data, err := s.Next()
		if err != nil {
			log.Printf("error reading file %s : %s", s.Filename, err)
			finished = true
			break
		}

		ticket, _ := processors.NewTicket(data)
		_ = ticket

		count++
		//fmt.Println(dic)
	}

	elapsed := time.Since(start)
	fmt.Printf("decode [%d] lines in %s", count, elapsed)

}

func feedStat(category string, stats map[string]map[string]int, ticket *processors.TicketInes) {

	// create category if necessary
	if _, ok := stats[category]; ok == false {
		stats[category] = make(map[string]int)
	}
	// count fields
	field, ok := ticket.Tags[category]
	if ok == true {
		_, ok := stats[category][field]
		if ok == true {
			stats[category][field] += 1
		} else {
			stats[category][field] += 1
		}
	}
}

func TestStatsOnTicketInes(t *testing.T) {

	//filename := "../../test/samples/rtlog_10.log"
	//filename := "../../samples/LogFonct.log-20191005"
	filename := "../../samples/LogFonct.log-20191006"

	stats := make(map[string]map[string]int)
	/*
		stats["LogType"]= make(map[string]int)
		stats["MessType"]= make(map[string]int)
		stats["InformationalSystem"]= make(map[string]int)
		stats["ClientOption"]= make(map[string]int)
	*/
	//ClientOption

	start := time.Now()

	s, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Printf("%s", err.Error())
		t.Fail()
		return
	}

	count := 0
	for finished := false; finished == false; {
		data, err := s.Next()
		if err != nil {
			log.Printf("error reading file %s : %s", s.Filename, err)
			finished = true
			break
		}

		ticket, _ := processors.NewTicket(data)

		/*
			field ,ok := ticket.Tags["LogType"]
			if ok == true {
				 _,ok := stats["LogType"][field]
				 if ok == true {
					stats["LogType"][field] += 1
				} else {
					stats["LogType"][field] += 1
				}
			}

			field ,ok = ticket.Tags["MessType"]
			if ok == true {
				_,ok := stats["MessType"][field]
				if ok == true {
					stats["MessType"][field] += 1
				} else {
					stats["MessType"][field] += 1
				}
			}

			field ,ok = ticket.Tags["InformationalSystem"]
			if ok == true {
				_,ok := stats["InformationalSystem"][field]
				if ok == true {
					stats["InformationalSystem"][field] += 1
				} else {
					stats["InformationalSystem"][field] += 1
				}
			}
		*/
		feedStat("LogType", stats, &ticket)
		feedStat("MessType", stats, &ticket)
		feedStat("InformationalSystem", stats, &ticket)
		feedStat("ClientOption", stats, &ticket)

		_ = ticket

		count++
		//fmt.Println(dic)
	}

	elapsed := time.Since(start)
	fmt.Printf("decode [%d] lines in %s\n", count, elapsed)
	fmt.Printf("stats: %v\n", stats)

}
