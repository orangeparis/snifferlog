package processors

/*

	writers publish specific messages

	writers

	* receive ticket via Feed()
    * filter messages
	* emit messages via publisher



*/

type Writer interface {
	// feed the writer , return true if there is a match , false or err if not
	Feed(ticket TicketInes) (match bool, err error)
}
