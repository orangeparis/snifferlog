package snifferlog

import "encoding/json"

/*

	export an aggregat of logs (tickets) ( grouped by traceID )



	sniffer.log.user.start
	sniffer.log.user.stop

	sniffer.log.session.start.ipv4   ( ipv4 or ipv6 )
	sniffer.log.session.stop.ipv4    ( ipv4 or ipv6 )



	sniffer.log.other



*/

/*

"MessType":"Accounting Subscriber Interim"

- "Login"
- "LineID"
- "TraceID"

Dump aggregator TIDdff10ae1a85f8a5f7e9c71af16f0c

{	CLIENT

	"LogType":"FUNCT_CLIENT", "MessType":"Accounting Subscriber Interim",
	"Login":"fti/gw73dq7",
	"AcctProfile":"NO_PROFILE","Address":"","AllocationMode":"1",
	"BNGID":"NEE2E",
	"BNGIP":"172.20.210.253",
	"CircuitID":"UNKNOWN",
	"ClientAccessMode":"3","ClientOption":"FTTH","DeviceID":"",
	"ErrCode":"0","ErrMess":"Operation successful",
	"EventTimeStamp":"2019-10-05 02:01:27.000",
	"LineID":"00000011834009",

	"ProcTime":"32","QoSProfile":"NO_PROFILE","StateInd":"0",
	"TimeStamp":"2019-10-05 02:01:27.286",
	"TraceID":"TIDdff10ae1a85f8a5f7e9c71af16f0c"}


{	METEOR_TKT

	"LogType":"FUNCT_TICKET", "MessType":"Accounting Subscriber Interim",
	"InformationalSystem":"Meteor TKT",
	"Login":"fti/gw73dq7",

	"Address":"","BNGID":"NEE2E","BNGIP":"172.20.210.253","CircuitID":"UNKNOWN",
	"ErrCode":"0","ErrMess":"Operation successful","EventCause":"",
	"LineID":"00000011834009",
	"Serial":"99389d1601-6d50-00fd-3627-ebc0a8d307-502","StateInd":"0",
	"TimeStamp":"2019-10-05 02:01:27.321",
	"TraceID":"TIDdff10ae1a85f8a5f7e9c71af16f0c"}


{	MQ

	"LogType":"FUNCT_TICKET_MQ", "MessType":"MQ Series Ticket"
	"ErrCode":"0","ErrMess":"Operation successful",
	"MQSerial":"TID99389d2c01-6d50-00f3-830c-59c0a8d318",
	"StateInd":"0","TicketNBR":"1",
	"TimeStamp":"2019-10-05 02:01:27.321",
	"TraceID":"TIDdff10ae1a85f8a5f7e9c71af16f0c"}


{	GILDA_TKT

	"LogType":"FUNCT_TICKET", "MessType":"Accounting Subscriber Interim",
	"InformationalSystem":"Gilda TKT",

	"Address":"","BNGID":"NEE2E","BNGIP":"172.20.210.253","CircuitID":"UNKNOWN",
	"ErrCode":"0","ErrMess":"Operation successful",
	"EventCause":"0",

	"LineID":"00000011834009",
	"Login":"fti/gw73dq7",

	"Serial":"","StateInd":"0",
	"TimeStamp":"2019-10-05 02:01:30.422",
	"TraceID":"TIDdff10ae1a85f8a5f7e9c71af16f0c"}


{	GILDA_ACK

	"LogType":"FUNCT_TICKET",   "MessType":"Accounting Subscriber Interim",
	"InformationalSystem":"Gilda ACK",
	"Address":"","BNGID":"NEE2E","BNGIP":"172.20.210.253","CircuitID":"UNKNOWN",
	"ErrCode":"TICKET_MODULE_NO_RESP_RETRY","ErrMess":"Ticket Module is not responding : Timeout",
	"EventCause":"0",

	"LineID":"00000011834009",
	"Login":"fti/gw73dq7",

	"Serial":"","StateInd":"3",
	"TimeStamp":"2019-10-05 02:01:30.423",
	"TraceID":"TIDdff10ae1a85f8a5f7e9c71af16f0c"}


*/

var CommonFields = []string{"TraceID", "MessType", "MessType", "Login", "LineID", "EventTimeStamp"}

type RawAggregate struct {
	ID   string              // generally TraceID
	Data []map[string]string // accumulator of string map
}

// an aggregate of logs based on traceID
type Aggregate struct {
	TraceID string // eg "TIDdff10ae1a85f8a5f7e9c71af16f0c"

	MessType string // eg "Accounting Subscriber Interim"
	Login    string // eg "fti/gw73dq7"
	LineID   string // eg "00000011834009"

	EventTimeStamp string // from FUNCT_CLIENT    eg "2019-10-05 02:01:27.000"

	LogCount int // number of log in logs
	Status   int // +1000 if CLIENT, +100 if METEOR TKT, + 10 if GILDA_TKT , +1 if GILDA_ACK

	FootPrint Category // a string representing an hex  ( eg 'F0' )

	//Logs []map[string]string
	Logs []TicketTags
}

func NewAggregate(traceID string) *Aggregate {
	a := &Aggregate{TraceID: traceID}
	return a
}

func (a *Aggregate) Size() int {
	return len(a.Logs)
}

func (a *Aggregate) Add(tags map[string]string) {

	tt := TicketTags(tags)
	if x := tt.Login(); x != "" {
		a.Login = x
	}
	if x := tt.LineID(); x != "" {
		a.LineID = x
	}

	x := tt.LogType()
	if x == "FUNCT_TICKET" || x == "FUNCT_CLIENT" {
		if x := tt.MessType(); x != "" {
			a.MessType = x
		}
	}

	if x := tt.EventTimeStamp(); x != "" {
		a.EventTimeStamp = x
	}

	a.Logs = append(a.Logs, tags)
	a.LogCount += 1
	a.Status += tt.GetCategory()

	a.FootPrint.Merge(tt.GetCategoryByte())

}

func (a *Aggregate) AddMap(logs []map[string]string) {

	for _, v := range logs {
		a.Add(v)
	}

}

func (a *Aggregate) Json() (data []byte, err error) {
	return json.Marshal(a)
}

func (a *Aggregate) Show() (data []byte, err error) {
	return json.MarshalIndent(a, "", "  ")
}
