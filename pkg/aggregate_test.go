package snifferlog

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	"log"
	"testing"
)

func TestAggregate(t *testing.T) {

	// open file to read tickets
	filename := "../samples/LogFonct.log-20191005"

	//source, err := sources.NewFileSource(filename, "3.3ms")
	source, err := sources.NewFileSource(filename, "1ms")
	//source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		t.Fail()
		return
	}
	defer source.Close()

	// reading ticket loop
	traceID := ""
	var agg *Aggregate
	n := 1
	for finished := false; finished == false; {
		data, err := source.Next()
		if err != nil {
			log.Printf("error reading Source %s : %s", source.Name(), err)
			finished = true
			break
		}

		ticket, err := NewTicket(data)
		if err != nil {
			log.Printf("Parser: failed to parse ticket (%s): %s\n", err.Error(), string(data))
			//p.PublishError(err.Error(), string(data))
			return
		}

		if n == 1 {
			// this is our first ticket create aggregate and sage TraceID
			id := ticket.GetTraceID()
			if id == "" {
				t.Fatalf("Cannot get traceid")
			}
			traceID = id
			agg = NewAggregate(traceID)
			agg.Add(ticket.Tags)
			n += 1
			continue
		}
		// other ticket ( only want same tid
		id := ticket.GetTraceID()
		if id == "" {
			t.Fatalf("Cannot get traceid")
		}
		if id != traceID {
			// skip it
			n += 1
			if n >= 200 {
				//t.Log("ok")
				show, _ := agg.Show()
				println(string(show))
				return
			}
			continue
		}

		// add this ticket to aggregate
		agg.Add(ticket.Tags)

		n += 1

	}

	println("Done.")

}
