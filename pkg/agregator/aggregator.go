package agregator

import (
	publishers "bitbucket.org/orangeparis/ines/publisher"
	snifferlog "bitbucket.org/orangeparis/snifferlog/pkg"
	"context"
	"io"
	"log"
	"os"
	"time"
)

/*

	aggregators accumulates maps in a buffer until a timeout

*/

type Aggregator interface {
	Run(ctx context.Context)
	Write(string, map[string]string)
}

type GenericAggregator struct {
	Id    string
	Input chan map[string]string

	Aggregate *snifferlog.Aggregate

	Publisher publishers.Publisher

	Closed   bool
	Duration time.Duration // duration of the aggregation
	Count    int           // count number of log registred

	exit func(string)

	// exporter Exporter

}

func NewGenericAggregator(id string, duration time.Duration, exit func(string), pub publishers.Publisher) (
	a *GenericAggregator, err error) {

	a = &GenericAggregator{
		Id:    id,
		Input: make(chan map[string]string),
		//Buffer: make( []map[string]string ),
		Aggregate: snifferlog.NewAggregate(id),
		Publisher: pub,
		Duration:  duration,
		exit:      exit,
	}
	return
}

//
// implements Aggregator interface
//
func (a *GenericAggregator) Write(tag string, entry map[string]string) {
	if a.Closed == false {
		a.Input <- entry
	} else {
		log.Printf("aggregator: input is closed")
	}
}

func (a *GenericAggregator) Run(context.Context) {

	//log.Printf("Aggregator [%s] started\n", a.Id)
	ticker := time.NewTicker(a.Duration)
	count := 0
	defer ticker.Stop()
	for {
		select {

		case e := <-a.Input:
			//a.Buffer = append(a.Buffer, e)
			a.Aggregate.Add(e)

		//case ticker.
		case <-ticker.C:
			ticker.Stop()
			//ticker = time.NewTicker(time.Duration(start_interval/counter) * time.Millisecond)
			count += 1
			//log.Printf("aggregator [%s] exited\n", a.Id)
			a.Closed = true
			close(a.Input)
			a.Dump(os.Stdout)
			a.exit(a.Id)
			return

			//case <-time.After(a.Duration):
			//	//log.Printf("aggregator [%s] exited\n", a.Id)
			//	a.Closed = true
			//	close(a.Input)
			//	a.Dump(os.Stdout)
			//	a.exit(a.Id)
			//	return
		}
	}
	//time.Sleep(a.Duration)
	log.Printf("aggregator exited")

}

// utility function
func (a *GenericAggregator) Dump(w io.Writer) (err error) {

	result, err := a.Aggregate.Json()
	if err != nil {
		log.Printf("Generic aggregator Dump error:%s\n", err.Error())
		return err
	}
	identifier := "unknown"
	if a.Aggregate.LineID != "" {
		identifier = a.Aggregate.LineID
	} else {
		if a.Aggregate.Login != "" {
			identifier = a.Aggregate.Login
		}
	}
	a.Publisher.Publish(identifier, []byte(result))
	//if a.Aggregate.Size() >= 4 {
	//w.Write([]byte(result))
	//w.Write([]byte("\n"))
	//}
	return

	//var outs []string
	//if len(a.Buffer) >= 1 {
	//
	//	outs = append(outs, fmt.Sprintf("Dump aggregator %s", a.Id))
	//	for _, e := range a.Buffer {
	//		out, err := json.Marshal(e)
	//		if err == nil {
	//			outs = append(outs, string(out))
	//		} else {
	//			outs = append(outs, `nul`)
	//		}
	//	}
	//	result := strings.Join(outs, "\n")
	//	result += "\n"
	//
	//	w.Write([]byte(result))
	//}
	//return
}
