package agregator

import (
	publisher "bitbucket.org/orangeparis/ines/publisher"
	parser "bitbucket.org/orangeparis/snifferlog/pkg"
	"context"
	"log"
	"sync"
	"time"
)

// a map of aggregators
type AggregatorMap struct {
	Ctx context.Context

	mux      sync.Mutex
	Map      map[string]Aggregator
	Duration time.Duration

	// a channel of id ( sent by the aggregator to tell they are out )
	ClosedAggregator chan string

	// publisher
	Publisher publisher.Publisher
}

func NewAggregatorMap(pub publisher.Publisher, duration time.Duration) (m *AggregatorMap) {
	m = &AggregatorMap{
		Ctx:              context.Background(),
		Map:              make(map[string]Aggregator),
		Duration:         duration,
		ClosedAggregator: make(chan string, 128),
		Publisher:        pub,
	}
	return
}

// create a new aggregator
func (m *AggregatorMap) New(id string, duration time.Duration, pub publisher.Publisher) (a Aggregator, err error) {

	return NewGenericAggregator(id, duration, m.Delete, pub)

}

//
//	implements Ticket Writer
//

func (m *AggregatorMap) Write(id string, ticket parser.TicketInes) {

	entry := ticket.Tags
	traceID, ok := entry["TraceID"]
	if ok != true {
		log.Printf("No trace ID for ticket:%s\n", entry)
		return
	}

	// search if aggregator for id exists
	m.mux.Lock()
	a, ok := m.Map[traceID]
	if ok == false {
		// aggregator does not exists : create , run and feed
		n, _ := m.New(traceID, m.Duration, m.Publisher)
		m.Map[traceID] = n
		m.mux.Unlock()
		go n.Run(m.Ctx)
		//time.Sleep(1 * time.Millisecond)
		n.Write("topic", entry)
	} else {
		// feed the aggregator
		m.mux.Unlock()
		a.Write("topic", entry)
	}
}

func (m *AggregatorMap) Run(ctx context.Context) {

	for {
		// a task to delete closed aggregators
		select {
		case agId := <-m.ClosedAggregator:
			m.mux.Lock()
			//log.Printf("aggregatorMap deletes entry: %s", agId)
			delete(m.Map, agId)
			m.mux.Unlock()
		}
	}
}

func (m *AggregatorMap) Delete(agId string) {

	m.ClosedAggregator <- agId
}
