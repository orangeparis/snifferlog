package agregator

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	"bitbucket.org/orangeparis/ines/publisher"
	parser "bitbucket.org/orangeparis/snifferlog/pkg"
	"fmt"
	"log"
	"testing"
	"time"
)

var data = []map[string]string{
	map[string]string{"id": "A", "key": "value1"},
	map[string]string{"id": "B", "key": "value2"},
	map[string]string{"id": "A", "key": "value3"},
	map[string]string{"id": "A", "key": "value4"},
	map[string]string{"id": "C", "key": "value5"},
	map[string]string{"id": "A", "key": "value6"},
}

func TestAggregatorMap(t *testing.T) {

	pub := publisher.NewScreenPublisher("sniffer.log")

	am := NewAggregatorMap(pub, 5*time.Second)
	go am.Run(am.Ctx)

	for _, entry := range data {
		id := entry["id"]
		ticket := &parser.TicketInes{Tags: entry}

		am.Write(id, *ticket)
	}

	time.Sleep(2 * time.Second)

	time.Sleep(6 * time.Second)

	print("Done")

}

func TestAggregatorMapAsATicketWriter(t *testing.T) {

	start := time.Now()

	pub := publisher.NewScreenPublisher("sniffer.log")
	//pub := publisher.NewNilPublisher()

	am := NewAggregatorMap(pub, 15*time.Second)
	go am.Run(am.Ctx)

	filename := "../../samples/LogFonct-ines-short.log"

	//source, err := sources.NewFileSource(filename, "3.3ms")
	source, err := sources.NewFileSource(filename, "1ms")
	//source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		t.Fail()
		return
	}

	parser.Parse(source, am)

	elapsed := time.Since(start)
	fmt.Printf("elapsed time: %s\n", elapsed)

	time.Sleep(2 * time.Second)
	time.Sleep(6 * time.Second)

	print("Done")

}
