package snifferlog

import (
	"fmt"
)

/*
	category



*/

type Category byte

func (c *Category) Merge(category Category) (cat *Category) {
	*c = *c | category
	return c
}

func (c *Category) String() (s string) {

	return fmt.Sprintf("%X", *c)

}
