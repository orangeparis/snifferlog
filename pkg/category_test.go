package snifferlog

import "testing"

func TestCategory(t *testing.T) {

	x1 := Category(0x80)
	x2 := Category(0x40)
	x3 := Category(0x20)
	x4 := Category(0x10)

	status := x1.Merge(x2).Merge(x3).Merge(x4)

	s := status.String()
	if s != "F0" {
		t.Fail()
	}

	println(s)

}
