package snifferlog

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
)

/*

	a nats sniffer log  subscriber

	implements TicketWriter interface

		type TicketWriter interface {
			Write(topic string, ticket TicketInes)
}



*/

type NatsSnifferLogSubscriber struct {
	*nats.Conn

	topic string
	//aggTime time.Duration

}

func NewNatsSnifferLogSubscriber(natsURL string, topic string) (s *NatsSnifferLogSubscriber, err error) {

	// create nats connection
	nc, err := nats.Connect(natsURL)
	if err != nil {
		log.Printf("snifferlog subscriber: cannot open nats connection: %s", err.Error())
		return
	}
	//defer nc.Close()
	msg := fmt.Sprintf("snifferlog subscriber connected with nats at : %s\n", natsURL)
	log.Println(msg)

	// topic := "sniffer.log." + lineID

	s = &NatsSnifferLogSubscriber{
		Conn:  nc,
		topic: topic,
		//aggTime: time.Duration(aggtime) * time.Second,
	}
	return

}

func (s *NatsSnifferLogSubscriber) Run(ctx context.Context) {

	// subscribe to all inputs
	sub, err := s.Subscribe(s.topic, func(m *nats.Msg) {
		//log.Printf("snifferlog subscriber caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")
		s.HandleMessage(m)

	})
	if err != nil {
		log.Printf("")
	}

	select {

	case <-ctx.Done():
		// asked for shutdown
		sub.Unsubscribe()
		s.Close()
		return
	}

}

func (s *NatsSnifferLogSubscriber) HandleMessage(msg *nats.Msg) {

	data := msg.Data

	// convert to an aggregate (of tickets )
	agg := NewAggregate("")

	err := json.Unmarshal(data, &agg)
	if err != nil {
		log.Printf("Failed to unmarshal: %s\n", err)
		return
	}

	m, err := agg.Show()
	if err == nil {
		println(string(m))
	}

	return

}
