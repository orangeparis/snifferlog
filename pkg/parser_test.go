package snifferlog

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	"log"
	"testing"
)

func TestParser(t *testing.T) {

	filename := "../samples/LogFonct.log-20191005"

	source, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Println(err)
		t.Fail()
		return
	}
	dest := ScreenTicketWriter{}

	Parse(source, dest)

	println("done")

}
