package snifferlog

import "strings"

/*

	ticket tags

		TimeStamp="2019-10-05 01:00:51.116" TraceID="TIDf2cf0e6378619636e0f56616d109f"
		LogType="FUNCT_TICKET"
		MessType="Accounting Subscriber Stop" StateInd="0" ErrCode="0" ErrMess="Operation successful"
		InformationalSystem="Meteor TKT"

*/

// log category
const (
	CLIENT     = 1000
	METEOR_TKT = 100
	GILDA_TKT  = 10
	GILDA_ACK  = 1
)

type TicketTags map[string]string

// find traceID
func (t TicketTags) TraceID() string { // TID......
	return t["TraceID"]
}
func (t TicketTags) LogType() string { // FUNCT_CLIENT ...
	return t["LogType"]
}
func (t TicketTags) MessType() string { // "Accounting Host IPv4 Stop" ...
	return t["MessType"]
}

// InformationalSystem
func (t TicketTags) InformationalSystem() string { // "Meteor TKT"  ...
	return t["InformationalSystem"]
}

// Login
func (t TicketTags) Login() string { // "fti/12345"  ...
	return t["Login"]
}

// LineID
func (t TicketTags) LineID() string { // "00000249207802" ...
	return t["LineID"]
}

// EventTimeStamp="2019-10-05 01:00:56.000"
func (t TicketTags) EventTimeStamp() string { // "2019-10-05 01:00:56.000" ...
	return t["EventTimeStamp"]
}

// analyse type
func (t TicketTags) GetCategory() int {

	if t.LogType() == "FUNCT_CLIENT" {
		return CLIENT
	}
	if t.LogType() == "FUNCT_TICKET" {
		info := t.InformationalSystem()
		info = strings.Replace(info, "_", " ", 1)
		info = strings.ToUpper(info)
		switch info {
		case "METEOR TKT":
			return METEOR_TKT
		case "GILDA TKT":
			return GILDA_TKT
		case "GILDA ACK":
			return GILDA_ACK
		default:
			return 0
		}
	}
	return 0
}

// analyse type
func (t TicketTags) GetCategoryByte() (cat Category) {

	if t.LogType() == "FUNCT_CLIENT" {
		return 0x80
	}
	xtype := t.LogType()            // FUNCT_*
	info := t.InformationalSystem() // METEOR TKT, MQ_SERIE
	info = strings.Replace(info, "_", " ", -1)
	info = strings.ToUpper(info)

	switch xtype {

	case "FUNCT_TICKET":
		switch info {
		case "METEOR TKT":
			return 0x40
		case "GILDA TKT":
			return 0x20
		case "GILDA ACK":
			return 0x10
		default:
			return 0
		}

	case "FUNCT_TICKET_MQ":
		mtype := t.MessType() // METEOR TKT, MQ_SERIE
		mtype = strings.Replace(mtype, "_", " ", -1)
		mtype = strings.ToUpper(mtype)

		switch mtype {
		case "MQ SERIES TICKET":
			return 0x08
		default:
			return 0x0
		}
	case "FUNCT_SYNC":
		return 0x04
	}
	return 0
}
