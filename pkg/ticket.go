package snifferlog

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strings"
)

/*

	a parser for rtlog ticket:  timestamp origin INES: (key="value" )*

	sample ticket:

2019-10-05T01:00:52.431+02:00 innapnoi3fed06 INES: TimeStamp="2019-10-05 01:00:52.424" EventTimeStamp="2019-10-05 01:00:50.000" TraceID="TID6ab9e2e333e615cb02664562a98ef" LogType="FUNCT_CLIENT" MessType="Accounting Subscriber Interim" ProcTime="7" StateInd="2" ErrCode="7" ErrMess="Connection not found in Database" ClientAccessMode="3" Login="" DeviceID="" Address="" AllocationMode="" BNGID="NESONDE" BNGIP="172.20.210.253" AcctProfile="NO_PROFILE" QoSProfile="NO_PROFILE" LineID="00000249207802" CircuitID="UNKNOWN" ClientOption=""

bad ticket header
<13>Feb 26 18:56:54 innapnoi6bed06 INES:



*/

var HeaderReg = regexp.MustCompile(`(?P<timestamp>.+) (?P<origin>[a-zA-Z0-9_\-]+) INES:(?P<kv>.*)`)
var kvReg = regexp.MustCompile(`(?P<key>\w+)=\"(?P<value>.*?)\"`)

type TicketHeader struct {
	TimeStamp string // timestamp of the trace  eg   2019-10-05T01:00:53.722+02:00
	Origin    string // machine originating the log eg : innapnoi3bed02
	Marker    string // mark the start of key/values  eg  'INES:'
	// 2019-10-05T01:00:53.722+02:00 innapnoi3bed02 INES:
}

// ticket ines , format:  timestamp origin INES: (key="value" )*
type TicketInes struct {
	Line   string
	Header *TicketHeader
	Tags   map[string]string
	Err    error
	cursor int
}

// create an rtlog ticket from data
func NewTicket(data []byte) (ticket TicketInes, err error) {

	//header := &TicketHeader{}
	//tags := make(map[string]string)

	ticket = TicketInes{Line: string(data), Header: &TicketHeader{}, Tags: make(map[string]string)}

	//(?P<timestamp>.+) (?P<origin>[a-zA-Z0-9_\-]+) INES:.*`
	err = ticket.Parse()
	return ticket, err
}

func (t TicketInes) Parse() (err error) {

	err = t.ParseHeader()
	if err != nil {
		return err
	}

	err = t.ParseTags()
	if err != nil {
		return err
	}
	return nil
}

//
func (t *TicketInes) ParseHeader() error {

	if len(t.Line) == 0 {
		err := errors.New("empty line")
		return err
	}

	text := t.Line
	header := t.Header
	state := 0

	// test the crappy header `<13>1 `
	if text[0] == '<' {
		// we got the crappy header let's skip it
		t.cursor = 6
	}

	b0 := t.cursor

forloop:
	for i := t.cursor; i < len(t.Line); i++ {
		switch state {

		case 0:
			// process Timestamp
			if text[i] == ' ' {
				// extract timestamp
				header.TimeStamp = text[b0:i]
				b0 = i
				state = 1
			}
		case 1:
			// process log originator
			if text[i] == ' ' {
				// extract origin
				header.Origin = text[b0+1 : i]
				b0 = i
				state = 2
			}

		case 2:
			// process marker
			if text[i] == ' ' {
				// extract marker
				header.Marker = text[b0+1 : i]
				t.cursor = i + 1
				break forloop
			}
		}
	}
	if len(t.Header.Marker) >= 4 {
		if t.Header.Marker[:4] != "INES" {
			err := errors.New("Bad Header")
			return err
		}
	} else {
		// bad ines marker in header
		msg := fmt.Sprintf("Bad INES marker in header: %s\n", text)
		err := errors.New(msg)
		return err
	}
	return nil
}

func (t *TicketInes) ParseTags() error {

	//data := make(map[string]string)

	text := t.Line

	var state = 0

	var k0 = t.cursor
	var k1 = t.cursor
	var v0 = t.cursor
	var v1 = t.cursor

	var lastKey = ""

	state = 1
	for i := t.cursor; i < len(text); i++ {

		switch state {

		case 0:
			// process blank separating 2 kv entries
			if text[i] == ' ' {
				k0 = i + 1
				k1 = k0
				v0 = 1
				v1 = 1
				state = 1
			}

		case 1:
			// process key  [a-zA-Z0-9]+
			if text[i] == '=' {
				// we detect end of key : remember end 0f key k1
				k1 = i
				lastKey = text[k0:k1]
				//fmt.Printf("key: %s\n", text[k0:k1])
				//out = append(out, text[k0:k1])
				state = 2
			} else if text[i] == ' ' {
				// false start : this is not a key
				k0 = i + 1
				k1 = k0
				v0 = 1
				v1 = 1
				state = 1
			}

		case 2:
			//  process  "
			if text[i] == '"' {
				// we detect start of value
				v0 = i + 1
				v1 = v0
				state = 3
			}
		case 3:
			// process value
			if text[i] == '"' {
				// we detect the end of value
				v1 = i
				//t.Tags[strings.TrimSpace(lastKey)] = text[v0:v1]
				t.Tags[lastKey] = text[v0:v1]
				//fmt.Printf("value: %s\n", text[v0:v1])
				//out = append(out, text[v0:v1])
				state = 0
			}
		}
	}
	if state != 0 {
		// bad key value format
		err := errors.New("bad key/value format")
		return err
	}
	return nil

}

// tojson : json representation of a ticket
func (t *TicketInes) ToJson() (data []byte, err error) {

	// build and send messages
	data, err = json.Marshal(t.Tags)
	if err != nil {
		//log.Printf("TicketInes: cannot marshal ticket:%s\n", err)
		return data, err
	}
	return data, err
}

// find traceID
func (t *TicketInes) GetTraceID() string { // TID......
	return t.Tags["TraceID"]
}
func (t *TicketInes) GetLogType() string { // FUNCT_CLIENT ...
	return t.Tags["LogType"]
}
func (t *TicketInes) GetMessType() string { // "Accounting Host IPv4 Stop" ...
	return t.Tags["MessType"]
}

// InformationalSystem
func (t *TicketInes) GetInformationalSystem() string { // "Meteor TKT"  ...
	return t.Tags["InformationalSystem"]
}

// Login
func (t *TicketInes) GetLogin() string { // "fti/12345"  ...
	return t.Tags["Login"]
}

// LineID
func (t *TicketInes) GetLineID() string { // "00000249207802" ...
	return t.Tags["LineID"]
}

func (t *TicketInes) RegexParser() (err error) {

	/*

		the canonical header is like :

		2019-10-05T01:00:52.431+02:00 innapnoi3fed06 INES:

		but we need to handle other headers like

		<13>Feb 26 18:56:54 innapnoi6bed06 INES:


	*/
	loc := HeaderReg.FindStringSubmatchIndex(t.Line)
	if loc == nil {
		err := errors.New("bad header")
		return err
	}
	// decode header
	t.Header.TimeStamp = t.Line[loc[2]:loc[3]]
	t.Header.Origin = t.Line[loc[4]:loc[5]]
	t.Header.Marker = "INES:"
	//kvStart := loc[6]
	kv := t.Line[loc[6]:loc[7]]

	//println(kv)
	withregex := false

	if withregex == true {
		// handle kv with regexp

		lkv := kvReg.FindAllStringSubmatchIndex(kv, -1)
		if lkv == nil {
			err := errors.New("bad key/value format")
			return err
		}

		for _, x := range lkv {
			key := kv[x[2]:x[3]]
			v := kv[x[4]:x[5]]
			t.Tags[key] = v
		}
	} else {
		// handle kv with custom parser
		t.cursor = loc[6]
		t.ParseTags()
	}
	return
}

func (t *TicketInes) ParseHeader2() (err error) {

	/*
		the canonical header is like :

		2019-10-05T01:00:52.431+02:00 innapnoi3fed06 INES:

		but we need to handle other headers like

		<13>Feb 26 18:56:54 innapnoi6bed06 INES:

	*/

	sp := strings.Split(t.Line, "INES:")
	_ = sp

	return
}
