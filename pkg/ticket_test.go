package snifferlog

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	"encoding/json"

	"fmt"
	"log"
	"testing"
	"time"
)

func TestDecodeTicketInes(t *testing.T) {

	//filename := "../../test/samples/rtlog_10.log"
	filename := "../samples/LogFonct.log-20191005"

	start := time.Now()

	s, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Printf("%s", err.Error())
		t.Fail()
		return
	}

	count := 0
	for finished := false; finished == false; {
		data, err := s.Next()
		if err != nil {
			log.Printf("error reading file %s : %s", s.Filename, err)
			finished = true
			break
		}

		ticket := TicketInes{Line: string(data), Header: &TicketHeader{}, Tags: make(map[string]string)}
		ticket.Parse()
		_ = ticket

		count++
		//fmt.Println(dic)
	}

	elapsed := time.Since(start)
	fmt.Printf("decode [%d] lines in %s", count, elapsed)

}

func TestTicketInesRegexParser(t *testing.T) {

	//filename := "../../test/samples/rtlog_10.log"
	filename := "../samples/LogFonct.log-20191005"

	start := time.Now()

	s, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Printf("%s", err.Error())
		t.Fail()
		return
	}

	count := 0
	for finished := false; finished == false; {
		data, err := s.Next()
		if err != nil {
			log.Printf("error reading file %s : %s", s.Filename, err)
			finished = true
			break
		}

		ticket := TicketInes{Line: string(data), Header: &TicketHeader{}, Tags: make(map[string]string)}
		ticket.RegexParser()
		_ = ticket

		count++
		//fmt.Println(dic)
	}

	elapsed := time.Since(start)
	fmt.Printf("decode [%d] lines in %s", count, elapsed)

}

func feedStat(category string, stats map[string]map[string]int, ticket *TicketInes) {

	// create category if necessary
	if _, ok := stats[category]; ok == false {
		stats[category] = make(map[string]int)
	}
	// count fields
	field, ok := ticket.Tags[category]
	if ok == true {
		_, ok := stats[category][field]
		if ok == true {
			stats[category][field] += 1
		} else {
			stats[category][field] += 1
		}
	}
}

func TestStatsOnTicketInes(t *testing.T) {

	//filename := "../../test/samples/rtlog_10.log"
	//filename := "../../samples/LogFonct.log-20191005"
	filename := "../samples/LogFonct.log-20191006"

	stats := make(map[string]map[string]int)
	/*
		stats["LogType"]= make(map[string]int)
		stats["MessType"]= make(map[string]int)
		stats["InformationalSystem"]= make(map[string]int)
		stats["ClientOption"]= make(map[string]int)
	*/
	//ClientOption

	start := time.Now()

	s, err := sources.NewFileSource(filename, "")
	if err != nil {
		log.Printf("%s", err.Error())
		t.Fail()
		return
	}

	count := 0
	for finished := false; finished == false; {
		data, err := s.Next()
		if err != nil {
			log.Printf("error reading file %s : %s", s.Filename, err)
			finished = true
			break
		}

		ticket, _ := NewTicket(data)

		tags := TicketTags(ticket.Tags)
		fp := tags.GetCategoryByte()
		_ = fp

		feedStat("LogType", stats, &ticket)
		feedStat("MessType", stats, &ticket)
		feedStat("InformationalSystem", stats, &ticket)
		feedStat("ClientOption", stats, &ticket)

		_ = ticket

		count++
		//fmt.Println(dic)
	}

	elapsed := time.Since(start)
	fmt.Printf("decode [%d] lines in %s\n", count, elapsed)

	j, err := json.MarshalIndent(stats, "", " ")
	if err != nil {
		log.Fatalf("%s\n", err.Error())
	}
	println(string(j))

	//fmt.Printf("stats: %v\n", stats)

}

func TestParseTicketInes(t *testing.T) {

	var lines = []string{
		`2019-10-05T01:00:52.431+02:00 innapnoi3fed06 INES: TimeStamp="2019-10-05 01:00:52.424" EventTimeStamp="2019-10-05 01:00:50.000" TraceID="TID6ab9e2e333e615cb02664562a98ef" LogType="FUNCT_CLIENT" MessType="Accounting Subscriber Interim" ProcTime="7" StateInd="2" ErrCode="7" ErrMess="Connection not found in Database" ClientAccessMode="3" Login="" DeviceID="" Address="" AllocationMode="" BNGID="NESONDE" BNGIP="172.20.210.253" AcctProfile="NO_PROFILE" QoSProfile="NO_PROFILE" LineID="00000249207802" CircuitID="UNKNOWN" ClientOption=""`,
		`<13>1 2019-10-05T01:00:52.431+02:00 innapnoi6bed06 INES: TimeStamp="2019-10-05T01:00:52.431+02:00"`,
		`<13>1 2019-10-05T01:00:52.431+02:00 innapnoi6bed06 INES - - - TimeStamp="2019-10-05T01:00:52.431+02:00"`,
		`<13>1 2020-04-25T17:03:23+02:00 innapnoi6bed04 INES - - - TimeStamp="2020-04-25 17:03:23.170"`,
	}

	for _, data := range lines {

		ticket := TicketInes{Line: string(data), Header: &TicketHeader{}, Tags: make(map[string]string)}

		//ticket.RegexParser()
		ticket.Parse()

		continue

	}

}
