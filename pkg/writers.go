package snifferlog

import (
	"fmt"
)

type TicketWriter interface {
	Write(topic string, ticket TicketInes)
}

//
//  some Ticket writers
//

type ScreenTicketWriter struct{}

func (w ScreenTicketWriter) Write(topic string, ticket TicketInes) {
	fmt.Printf("%s:\n%s\n", topic, ticket.Tags)
}
