package players

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	"bitbucket.org/orangeparis/ines/publisher"
	//"bitbucket.org/orangeparis/snifferlog/internal/processors"
	snifferlog "bitbucket.org/orangeparis/snifferlog/pkg"
	"bitbucket.org/orangeparis/snifferlog/pkg/agregator"
	"time"

	"log"
)

// a redis pubsub publisher
type FilePlayer struct {
	source sources.DataSource

	publisher publisher.Publisher //  a nats or screen publisher to send  message

	//processor *processors.ProcessorInes

	Aggregator *agregator.AggregatorMap
	counter    int
}

func NewFilePlayer(rtlogFilename string, pub publisher.Publisher, aggtime int) (player *FilePlayer, err error) {

	aggregatorDuration := time.Duration(aggtime) * time.Second

	if pub == nil {
		pub = publisher.NewScreenPublisher("default.")
	}

	log.Printf("create new file player\n")

	//player = &FilePlayer{}

	source, err := sources.NewFileSource(rtlogFilename, "")
	if err != nil {
		log.Println(err)
		return
	}

	out := agregator.NewAggregatorMap(pub, aggregatorDuration)
	if err != nil {
		log.Println(err)
		return
	}

	//p, _ := processors.NewProcessor(source, pub, nil)
	//// add writers
	//p.AddWriter(processors.NewMeteorWriter(pub))
	//p.AddWriter(processors.NewClientWriter(pub))
	//p.AddWriter(processors.NewMqWriter(pub))

	player = &FilePlayer{
		source:    source,
		publisher: pub,
		//processor: p,
		Aggregator: out,
	}

	return player, err
}

func (player *FilePlayer) Close() {
	//
	//player.publisher.Close()
	//player.handle.Close()
	//log.Printf("snifferlog closed: %d lines processed\n", player.counter)
	return

}

func (player *FilePlayer) Start() {

	snifferlog.Parse(player.source, player.Aggregator)

	//ctx := context.Background()
	//player.processor.Run(ctx)

}
