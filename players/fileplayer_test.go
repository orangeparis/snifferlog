package players

import (
	"bitbucket.org/orangeparis/ines/publisher"
	"log"
	"testing"
	"time"
)

var (
	//pcapFile string = "../fixtures/ines_dhcp.pcap"
	//pcapFile string = "../fixtures/RADIUS.cap"
	//filename string = "../test/samples/sample_rtlog.log"
	filename string = "../samples/LogFonct-ines-short.log"

	//handle *pcap.Handle
	err error
)

func TestFilePlayer(t *testing.T) {

	pub := publisher.NewScreenPublisher("snifferlog.0")

	aggtime := 15

	player, err := NewFilePlayer(filename, pub, aggtime)
	if err != nil {
		log.Printf("error: %s", err.Error())
		return
	}
	player.Start()

	time.Sleep(5 * time.Second)

	//log.Printf("publisher has published : %d", publisher.Counter)

	return

}

//func TestFilePlayerWithMalformedRtlog(t *testing.T) {
//
//	var filename = "../test/samples/malformed_rtlog.log"
//
//	pub := publisher.NewScreenPublisher("snifferlog.rtlog")
//
//	player, err := NewFilePlayer(filename, pub)
//	if err != nil {
//		log.Printf("error: %s", err.Error())
//		return
//	}
//
//	//player.Start()
//	line := 0
//	for finished := false; finished == false; {
//		data, err := player.processor.Source.Next()
//		if err != nil {
//			log.Printf("error reading source %s : %s", player.processor.Source.Name(), err)
//			finished = true
//			break
//		}
//		line += 1
//		player.processor.RunOnce(data)
//
//	}
//
//	time.Sleep(5 * time.Second)
//
//	//log.Printf("publisher has published : %d", publisher.Counter)
//
//	return
//
//}
