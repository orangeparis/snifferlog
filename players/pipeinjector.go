package players

import (
	sources "bitbucket.org/orangeparis/ines/datasource"
	"log"
	"os"
	"time"
)

/*
	pipeinjector

	create a linux pipe and inject rtlog data from a file for testing pipeplayer


*/

type PipeInjector struct {
	*os.File                    // handler for pipe writing
	PipeName string             // pipe filename
	Source   sources.DataSource // data source to read from and inject
	Delay    time.Duration      // insert a delay after a Next() : to slow down the stream ,typically 10ms
}

func NewPipeInjector(pipeName string, source sources.DataSource, delay int) (inj *PipeInjector, err error) {

	// create pipe
	err = sources.CreateNamedPipe(pipeName)
	if err != nil {
		log.Printf("cannot create pipe %s: %s\n", pipeName, err.Error())
		return
	}
	// open pipe
	p, err := sources.GetPipeWriter(pipeName)
	if err != nil {
		log.Printf("cannot open pipe for read: %s\n", err.Error())
		return
	}

	inj = &PipeInjector{p, pipeName, source, 0}

	return
}

func (p *PipeInjector) Start() {

	// loop to read source and inject in pipe
	for {

		// read line from source
		line, err := p.Source.Next()
		if err != nil {
			log.Printf("next error: %s\n", err.Error())
			return
		}
		// write line to pipe
		_, err = p.File.WriteString(string(line))
		if err != nil {
			log.Printf("pipe writing error: %s\n", err.Error())
			return
		}
	}
}
