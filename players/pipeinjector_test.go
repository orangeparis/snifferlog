package players

import (
	//"bitbucket.org/orangeparis/snifferlog/internal/sources"
	sources "bitbucket.org/orangeparis/ines/datasource"
	"testing"
)

func TestPipeInjector(t *testing.T) {

	fname := "../test/samples/rtlog_10.log"
	pipeName := "/tmp/pipe.rtlog"

	// build source from file
	s, err := sources.NewFileSource(fname, "")
	if err != nil {
		t.Errorf("failed to open file source: %s", fname)
		return
	}
	defer s.Close()

	p, err := NewPipeInjector(pipeName, s, 0)
	if err != nil {
		t.Errorf("failed to create pipe injector: %s", err.Error())
		return
	}
	defer p.Close()

	// write all file content to pipe
	p.Start()

	//pub := publisher.NewScreenPublisher("snifferlog.0")
	//pipeReader , err := NewPipePlayer(pipeName,pub)
	//pipeReader.Start()

	// read pipe content
	p2, err := sources.NewPipeSource(pipeName)
	if err != nil {
		t.Errorf("failed to create pipeSource from : %s: %s", pipeName, err.Error())
		return
	}
	for i := 0; i < 2; i++ {

		line, err := p2.Next()
		if err != nil {
			t.Errorf("failed to read from  pipeSource from : %s: %s", pipeName, err.Error())
			return
		}
		println(string(line))
	}

}
