package test

import (
	"context"
	"testing"
)

func TestNewNatsSnifferLogSubscriber(t *testing.T) {

	natsURL := "nats://127.0.0.1:4222"
	lineID := "00000011569001"
	aggTime := 10

	s, err := NewNatsSnifferLogSubscriber(natsURL, lineID, aggTime)
	if err != nil {
		t.Fail()
		return
	}

	ctx := context.Background()
	s.Run(ctx)

}
