package test

import (
	"bitbucket.org/orangeparis/ines/publisher"
	"bitbucket.org/orangeparis/snifferlog/players"
	"log"
	"testing"
	"time"
)

/*

	publish nats events  sniffer.log.<lineID> from LogFonct-ines-short

*/

func TestSnifferLogEmitter(t *testing.T) {

	filename := "../samples/LogFonct-ines-short.log"
	aggregatorDuration := 10

	server := "nats://127.0.0.1:4222"
	pub, err := publisher.NewNatsPublisher("sniffer.log", server)
	if err != nil {
		log.Fatalf("%s\n", err.Error())
	}
	//pub := publisher.NewScreenPublisher("sniffer.log")

	player, err := players.NewFilePlayer(filename, pub, aggregatorDuration)
	if err != nil {
		log.Fatalf("%s\n", err.Error())
	}

	go player.Start()

	time.Sleep(10 * 60 * time.Second)

	println("Done")

}
